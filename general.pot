# $Id$
#
# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  lls.admin.inc: n/a
#  lls.module: n/a
#  lls.info: n/a
#  classes/LearnlineSearch.class.inc: n/a
#  templates/search-box.tpl.php: n/a
#  templates/search-latest.tpl.php: n/a
#  templates/search-result.tpl.php: n/a
#  templates/search-results-edmond.tpl.php: n/a
#  templates/search-result-edutags.tpl.php: n/a
#  templates/search-results-sidebar.tpl.php: n/a
#  templates/search-results-edmond-teaser.tpl.php: n/a
#  templates/search-results.tpl.php: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2015-07-04 00:40+0200\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: lls.admin.inc:25 lls.module:259
msgid "Advanced search"
msgstr ""

#: lls.admin.inc:26 lls.module:260
msgid "Defines the advanced search options either as plain HTML or as an array following the convetions of Drupal's Form API."
msgstr ""

#: lls.admin.inc:34 lls.module:268
msgid "Legal notice title"
msgstr ""

#: lls.admin.inc:35 lls.module:269
msgid "Shown as heading for the legal notice."
msgstr ""

#: lls.admin.inc:48 lls.module:282
msgid "Legal notice"
msgstr ""

#: lls.admin.inc:49 lls.module:283
msgid "Define the legal notice for search results, which is shown on top of the right sidebar."
msgstr ""

#: lls.admin.inc:60
msgid "Message if no results were found"
msgstr ""

#: lls.admin.inc:61
msgid "Define what to display if the current search request does not deliver any result.<br/> You can use %searchword as variable for the currently searched term."
msgstr ""

#: lls.admin.inc:68
msgid "Results per page"
msgstr ""

#: lls.admin.inc:69
msgid "Used for pagination."
msgstr ""

#: lls.admin.inc:93
msgid "Copyright logos directory"
msgstr ""

#: lls.admin.inc:94
msgid "Path to the directory where the CC images can be found (absolute or relative to Drupal's root directory).)"
msgstr ""

#: lls.module:117
msgid "Searching for"
msgstr ""

#: lls.module:130
msgid "learn:line NRW search configuration"
msgstr ""

#: lls.module:231
msgid "Sorry! We're temporarily unable to request the latest resources."
msgstr ""

#: lls.module:234
msgid "Latest SODIS resources"
msgstr ""

#: lls.module:459
msgid "%file:%line:\n %msg"
msgstr ""

#: lls.module:439
msgid "lls"
msgstr ""

#: lls.module:439
msgid "TypeAhead service not available"
msgstr ""

#: lls.module:30
msgid "learn:line specific settings"
msgstr ""

#: lls.module:31
msgid "Set up general search configuration."
msgstr ""

#: lls.module:44;53
msgid "List search results."
msgstr ""

#: lls.module:52
msgid "Search results"
msgstr ""

#: lls.info:0
msgid "learn:line Search"
msgstr ""

#: lls.info:0
msgid "Provides search and type-ahead functionality for the learn:line."
msgstr ""

#: lls.info:0
msgid "WebService Search"
msgstr ""

#: classes/LearnlineSearch.class.inc:161
msgid "Search term"
msgstr ""

#: classes/LearnlineSearch.class.inc:167
msgid "EDMOND identity"
msgstr ""

#: classes/LearnlineSearch.class.inc:182
msgid "Sorting criteria"
msgstr ""

#: classes/LearnlineSearch.class.inc:186
msgid "Sort order"
msgstr ""

#: classes/LearnlineSearch.class.inc:190
msgid "Resource context"
msgstr ""

#: classes/LearnlineSearch.class.inc:198
msgid "Disciplines"
msgstr ""

#: classes/LearnlineSearch.class.inc:210
msgid "Media types"
msgstr ""

#: classes/LearnlineSearch.class.inc:223
msgid "Publishers"
msgstr ""

#: classes/LearnlineSearch.class.inc:231
msgid "Copyright"
msgstr ""

#: classes/LearnlineSearch.class.inc:242
msgid "Languages"
msgstr ""

#: classes/LearnlineSearch.class.inc:253
msgid "Keywords"
msgstr ""

#: classes/LearnlineSearch.class.inc:264
msgid "Providers"
msgstr ""

#: classes/LearnlineSearch.class.inc:273
msgid "Competencies"
msgstr ""

#: classes/LearnlineSearch.class.inc:304
msgid "%class is missing setting '%setting'."
msgstr ""

#: classes/LearnlineSearch.class.inc:316
msgid "%class has no setting named '%setting'."
msgstr ""

#: templates/search-box.tpl.php:64;77
msgid "advanced search criteria"
msgstr ""

#: templates/search-latest.tpl.php:41 templates/search-result.tpl.php:74 templates/search-results-edmond.tpl.php:21
msgid "Media type"
msgstr ""

#: templates/search-latest.tpl.php:46 templates/search-result.tpl.php:80
msgid "File size"
msgstr ""

#: templates/search-latest.tpl.php:51
msgid "No description found."
msgstr ""

#: templates/search-result-edutags.tpl.php:19
msgid "Vote on Edutags website"
msgstr ""

#: templates/search-result-edutags.tpl.php:20
msgid "@voting of 100"
msgstr ""

#: templates/search-result-edutags.tpl.php:33
msgid "<em>1</em> tag"
msgstr ""

#: templates/search-result-edutags.tpl.php:34
msgid "%tags tags"
msgstr ""

#: templates/search-result-edutags.tpl.php:37
msgid "<em>1</em> comment"
msgstr ""

#: templates/search-result-edutags.tpl.php:38
msgid "%comments comments"
msgstr ""

#: templates/search-result-edutags.tpl.php:43
msgid "Edutags"
msgstr ""

#: templates/search-result-edutags.tpl.php:49
msgid "Tag this resource on Edutags website"
msgstr ""

#: templates/search-result-edutags.tpl.php:52
msgid "Tag this resource"
msgstr ""

#: templates/search-result-edutags.tpl.php:52
msgid "Add tags"
msgstr ""

#: templates/search-result-edutags.tpl.php:55
msgid "tagged @count times"
msgstr ""

#: templates/search-result-edutags.tpl.php:72
msgid "Comment on Edutags website"
msgstr ""

#: templates/search-result-edutags.tpl.php:75
msgid "Add new comment"
msgstr ""

#: templates/search-result-edutags.tpl.php:83
msgid "written by"
msgstr ""

#: templates/search-result-edutags.tpl.php:94
msgid "There are no comments on Edutags for this resource."
msgstr ""

#: templates/search-result.tpl.php:48
msgid "Toggle social share privacy bar."
msgstr ""

#: templates/search-result.tpl.php:50
msgid "share"
msgstr ""

#: templates/search-result.tpl.php:55
msgid "Toggle direct link box."
msgstr ""

#: templates/search-result.tpl.php:57
msgid "direct link"
msgstr ""

#: templates/search-result.tpl.php:64
msgid "Loading edutags ..."
msgstr ""

#: templates/search-result.tpl.php:67 templates/search-results-edmond.tpl.php:23
msgid "Publisher"
msgstr ""

#: templates/search-result.tpl.php:92 templates/search-results-sidebar.tpl.php:79
msgid "more"
msgstr ""

#: templates/search-result.tpl.php:100
msgid "Description"
msgstr ""

#: templates/search-result.tpl.php:107
msgid "School subject"
msgstr ""

#: templates/search-result.tpl.php:115;124
msgid "Not specified."
msgstr ""

#: templates/search-result.tpl.php:120
msgid "Typical educational age"
msgstr ""

#: templates/search-result.tpl.php:129
msgid "Usage notice"
msgstr ""

#: templates/search-results-edmond-teaser.tpl.php:17
msgid "Show all %count matches on Edmond NRW..."
msgstr ""

#: templates/search-results-edmond.tpl.php:20
msgid "Title"
msgstr ""

#: templates/search-results-edmond.tpl.php:22
msgid "Year"
msgstr ""

#: templates/search-results.tpl.php:36
msgid "Showing results"
msgstr ""

#: templates/search-results.tpl.php:38
msgid "of"
msgstr ""

#: templates/search-results.tpl.php:107
msgid "go to first page"
msgstr ""

#: templates/search-results.tpl.php:109
msgid "first"
msgstr ""

#: templates/search-results.tpl.php:111
msgid "go to previous page"
msgstr ""

#: templates/search-results.tpl.php:113
msgid "previous"
msgstr ""

#: templates/search-results.tpl.php:115
msgid "go to next page"
msgstr ""

#: templates/search-results.tpl.php:117
msgid "next"
msgstr ""

#: templates/search-results.tpl.php:119
msgid "go to last page"
msgstr ""

#: templates/search-results.tpl.php:121
msgid "last"
msgstr ""

