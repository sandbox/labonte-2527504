<?php
// $id:$

/**
 * @file
 * This file contains the module's autoloader and registers it with
 * spl_autoload_register().
 */

/**
 * Autoloader to register with spl_autoload_register().
 *
 * @param string $class
 *   Class (+ namespace) to be loaded.
 */
function _lls_autoload($class) {
  $class_path = explode('\\', $class);

  if (implode('\\', array_slice($class_path, 0, 2)) === 'publicplan\lls') {
    $module_dir = drupal_get_path('module', 'lls');
    $rel_path = implode('/', array_slice($class_path, 2));
    $file = $module_dir . DIRECTORY_SEPARATOR . 'classes' .
      DIRECTORY_SEPARATOR . $rel_path . '.class.inc';
    if (is_file($file) && is_readable($file)) {
      include_once $file;
    }
  }
}

spl_autoload_register('_lls_autoload');

/**
 * Include the SimpleSAMLphp autoloader.
 */
include DRUPAL_ROOT . '/../simplesamlphp/lib/_autoload.php';
//include DRUPAL_ROOT . '/../simplesamlphp/lib/_autoload_modules.php';
