<?php
// $Id:$

/**
 * @file
 * Module's admin settings.
 */

use publicplan\lls\LearnlineSearch;

// Facet mapping functions
require('facet_mapping/values.inc');
require('facet_mapping/mapping.inc');
require('facet_mapping/facets.inc');
require('school_locations/locations.inc');

/**
 * Rendered action theme for row actions.
 *
 * @param $operations List of available actions.
 * @return string Rendered action theme for row actions.
 * @throws \Exception
 */
function get_action_theme($operations) {

  $settings = array(
    '#theme' => 'links',
    '#links' => $operations,
    '#attributes' => array('class' => array('links lls-links')),
  );

  return drupal_render($settings);
}

/**
 * Callback function for general administration settings.
 *
 * @return array Renderable form.
 */
function lls_admin_settings() {
  $settings = array();

  $settings[LearnlineSearch::SETTING__RESULTS_PER_PAGE] = array(
    '#type' => 'textfield',
    '#title' => t('Results per page'),
    '#description' => t("Specify how many results should be listed per page."),
    '#default_value' => variable_get(LearnlineSearch::SETTING__RESULTS_PER_PAGE),
    '#weight' => 20,
  );

  $settings[LearnlineSearch::SETTING__CC_DIR] = array(
    '#type' => 'textfield',
    '#title' => t('Copyright logos directory'),
    '#description' => t("Path to the directory where the CC images can be found (absolute or relative to Drupal's root directory).)"),
    '#default_value' => variable_get(LearnlineSearch::SETTING__CC_DIR),
    '#weight' => 60,
  );

  $settings['dead_links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dead link reporting'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $settings['dead_links'][LearnlineSearch::SETTING__REPORT_DEAD_LINK_TARGET] = array(
    '#type' => 'textfield',
    '#title' => t('Email address for reporting dead links'),
    '#description' => t("The email address where to send user-reported dead links."),
    '#default_value' => variable_get(LearnlineSearch::SETTING__REPORT_DEAD_LINK_TARGET),
    '#weight' => 70,
  );

  $defaultValue = variable_get(LearnlineSearch::SETTING__REPORT_DEAD_LINK_MESSAGE);
  $settings['dead_links'][LearnlineSearch::SETTING__REPORT_DEAD_LINK_MESSAGE] = array(
    '#type' => 'text_format',
    '#format' => $defaultValue['format'],
    '#rows' => 25,
    '#title' => t('Link reported message'),
    '#description' => t('This text is shown to the user, when he reports a dead link.'),
    '#default_value' => $defaultValue['value'],
    '#weight' => 20,
  );

  $defaultValue = variable_get(LearnlineSearch::SETTING__REPORT_DEAD_LINK_CONFIRM_MESSAGE);
  $settings['dead_links'][LearnlineSearch::SETTING__REPORT_DEAD_LINK_CONFIRM_MESSAGE] = array(
    '#type' => 'text_format',
    '#format' => $defaultValue['format'],
    '#rows' => 10,
    '#title' => t('Confirmation message'),
    '#description' => t('This text is shown to the user, before he reports a dead link.'),
    '#default_value' => $defaultValue['value'],
    '#weight' => 20,
  );

  $settings['#submit'] = array('lls_admin_settings_submit');

  return system_settings_form($settings);
}

/**
 * Implements a custom submit handler.
 */
function lls_admin_settings_submit($form, &$form_state) {
  if (isset($form_state['values']) && !empty($form_state['values'])) {
    cache_clear_all();
    drupal_set_message('Cleared all caches.');
  }
}
