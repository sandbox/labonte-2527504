<?php
// $Id:$

/**
 * @file
 * Module's facet value management.
 */

use publicplan\lls\LearnlineSearch;

/**
 * Implements a school location overview.
 *
 * @return array Renderable form.
 * @throws \Exception
 */
function lls_school_locations() {

  drupal_set_title(t('Schulstandorte verwalten'));
  
  $rows = array();
  
  $q = db_select('school_location', 'sl');
  $q->addField('sl', 'location_id');
  $q->addField('sl', 'location_key');
  $q->addField('sl', 'location_value');
  $q->orderBy('sl.location_value', 'ASC');
  $result = $q->execute();
  
  while($res_obj = $result->fetchObject()) {
    
    $operations = array(
      'edit-details' => array(
        'title' => t('Edit'),
        'href' => 'admin/config/learnline/school-locations/edit/' . $res_obj->location_id,
      ),
      'delete' => array(
        'title' => t('Delete'),
        'href' => 'admin/config/learnline/school-locations/delete/' . $res_obj->location_id,
      ),
    );
    
    $rows[] = array(
      $res_obj->location_key,
      $res_obj->location_value,
      get_action_theme($operations),
    );
  }
  
  return array(
    '#theme' => 'table',
    '#header' => array(
      t('Kürzel'),
      t('Schulstandort'),
      t('Aktionen')
    ),
    '#rows' => $rows,
    '#empty' => t('Es sind keine Schulstandorte vorhanden.'),
  );
}

/**
 * Sets the value add page title and prepares the form.
 *
 * @return array Drupal form array.
 */
function lls_school_locations_add($id) {
  
  $type = ($id > 0) ? 'edit' : 'add';
  
  drupal_set_title(t(($type === 'add') ? 'Schulstandort hinzufügen' : 'Schulstandort bearbeiten'));
  return drupal_get_form('lls_school_locations_add_form');
}

/**
 * Value add form definition.
 *
 * @return array Renderable form.
 */
function lls_school_locations_add_form() {
  
  $id = arg(5);
  $location_key = '';
  $location_value = '';
  
  if(!empty($id)) {
    
    $q = db_select('school_location', 'sl');
    $q->addField('sl', 'location_key');
    $q->addField('sl', 'location_value');
    $q->condition('sl.location_id', $id);
    $result = $q->execute();
    
    if ($result->rowCount() > 0) {
      $res_obj = $result->fetchObject();
      $location_key = $res_obj->location_key;
      $location_value = $res_obj->location_value;
    }
  }
  
  $form['location_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Kürzel'),
    '#default_value' => $location_key,
    '#description' => '',
    '#required' => TRUE,
  );
  $form['location_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Schulstandort'),
    '#default_value' => $location_value,
    '#description' => '',
    '#required' => TRUE,
  );
  
  $form['id'] = array(
    '#type' => 'hidden',
    '#default_value' => $id,
    '#required' => FALSE,
  );
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t(($id > 0) ? 'Speichern' : 'Hinzufügen'),
    '#validate' => array('lls_school_locations_add_validate'),
    '#submit' => array('lls_school_locations_add_submit'),
  );
  
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#validate' => '',
    '#submit' => array('lls_school_locations_cancel'),
  );
  
  
  return $form;
}

/**
 * Callback for school location cancel form hook.
 */
function lls_school_locations_cancel($form, &$form_state) {
  $form_state['redirect'] = '/admin/config/learnline/school-locations';
}

/**
 * Callback for school location add form validation hook.
 */
function lls_school_locations_add_validate($form, &$form_state) {
  
  if(empty($form['id']['#value']) === true) {
    if($form['location_key']['#value'] !== $form['location_key']['#default_value']) {
      
      $q = db_select('school_location', 'sl');
      $q->addField('sl', 'location_id');
      $q->condition('sl.location_key', $form['location_key']['#value']);
      $q->range(0, 1);
  
      $result = $q->execute();
  
      if($result->rowCount() > 0) {
        form_set_error('location_key', t('Es ist bereits ein Wert mit diesem Kürzel vorhanden.'));
      }
    }
  }
}

/**
 * Callback for school location form submit hook.
 */
function lls_school_locations_add_submit($form, &$form_state) {
  
  $id = $form['id']['#value'];
  
  if(empty($id)) {
    
    db_insert('school_location')
      ->fields(array(
          'location_key' => $form['location_key']['#value'],
          'location_value' => $form['location_value']['#value'])
      )
      ->execute();
    
    drupal_set_message(t('Der Schulstandort wurde hinzugefügt.'));
  }
  else {
    
    db_update('school_location')
      ->fields(array(
          'location_key' => $form['location_key']['#value'],
          'location_value' => $form['location_value']['#value'],
        )
      )
      ->condition('location_id', $id)
      ->execute();
    
    drupal_set_message(t('Der Schulstandort wurde bearbeitet.'));
  }
  
  cache_clear_all();
  drupal_goto('/admin/config/learnline/school-locations');
}

/**
 * Implements the school location delete form.
 */
function lls_school_locations_delete($form, $form_state, $id) {
  
  $q = db_select('school_location', 'sl');
  $q->addField('sl', 'location_key');
  $q->addField('sl', 'location_value');
  $q->condition('sl.location_id', $id);
  $q->range(0, 1);
  
  $result = $q->execute();
  $res_obj = $result->fetchObject();
  
  return confirm_form(
    array(
      'id' => array(
        '#type' => 'value',
        '#value' => $id,
      ),
    ),
    t('Schulstandort löschen'),
    'admin/config/learnline/school-locations',
    t('Soll der Schulstandort <b>%value</b> wirklich gelöscht werden?', array('%value' => $res_obj->location_value . ' (' . $res_obj->location_key . ')')),
    t('Schulstandort löschen'),
    t('Abbrechen')
  );
}

/**
 * Implements delete logic for a school location.
 */
function lls_school_locations_delete_submit($form, &$form_state) {
  
  $id = $form_state['values']['id'];
  
  db_delete('school_location')
    ->condition('location_id', $id)
    ->execute();
  
  $form_state['redirect'] = '/admin/config/learnline/school-locations';
  
  drupal_set_message(t('Der Schulstandort wurde gelöscht.'));
}
