<?php
// $Id:$

/**
 * @file
 * Module's facet management.
 */

use publicplan\lls\SectionMapping;

/**
 * Implements a facet overview.
 *
 * @return array Renderable form.
 * @throws \Exception
 */
function lls_facet_facets() {

  drupal_set_title(t('Facetten verwalten'));

  $rows = array();

  $q = db_select(SectionMapping::DB_TABLE_TOP_SECTION, 'ts');
  $q->addField('ts', 'id');
  $q->addField('ts', 'value');
  $q->orderBy('ts.value', 'ASC');
  $result = $q->execute();

  while($res_obj = $result->fetchObject()) {

    $operations = array(
      'edit-details' => array(
        'title' => t('Edit'),
        'href' => 'admin/config/learnline/facet-mapping/edit/' . $res_obj->id,
      ),
      'delete' => array(
        'title' => t('Delete'),
        'href' => 'admin/config/learnline/facet-mapping/delete/' . $res_obj->id,
      ),
    );

    $rows[] = array(
      $res_obj->id,
      $res_obj->value,
      get_action_theme($operations),
    );
  }

  return array(
    '#theme' => 'table',
    '#header' => array(t('ID'), t('Facette'), t('Operations')),
    '#rows' => $rows,
    '#empty' => t('Es sind keine Facetten vorhanden.'),
  );
}

/**
 * Sets the facet add page title and prepares the form.
 *
 * @return array Drupal form array.
 */
function lls_facet_facets_add($id) {

  $type = ($id > 0) ? 'edit' : 'add';

  drupal_set_title(t(($type === 'add') ? 'Facette hinzufügen' : 'Facette bearbeiten'));
  return drupal_get_form('lls_facet_facets_add_form');
}

/**
 * Facet add form definition.
 *
 * @return array Renderable form.
 */
function lls_facet_facets_add_form() {

  $id = arg(5);
  $value = '';

  if(!empty($id)) {

    $q = db_select(SectionMapping::DB_TABLE_TOP_SECTION, 'ts');
    $q->addField('ts', 'value');
    $q->condition('ts.id', $id);
    $result = $q->execute();

    if ($result->rowCount() > 0) {
      $res_obj = $result->fetchObject();
      $value = $res_obj->value;
    }
  }

  $form['facet_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name der Facette'),
    '#default_value' => $value,
    '#description' => '',
    '#required' => TRUE,
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#default_value' => $id,
    '#required' => FALSE,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t(($id > 0) ? 'Speichern' : 'Hinzufügen'),
    '#validate' => array('lls_facet_facets_add_validate'),
    '#submit' => array('lls_facet_facets_add_submit'),
  );

  return $form;
}

/**
 * Callback for facet add form validation hook.
 */
function lls_facet_facets_add_validate($form, &$form_state) {

  if((empty($form['id']['#value']) && !empty($form['value']['#value'])) || ($form['id']['#value'] > 0 && $form['value']['#value'] !== $form['value']['#default_value'])) {

    $q = db_select(SectionMapping::DB_TABLE_TOP_SECTION, 'ts');
    $q->addField('ts', 'id');
    $q->condition('ts.id', $form['value']['#value']);
    $q->range(0, 1);

    $result = $q->execute();

    if($result->rowCount() > 0) {
      form_set_error('title', t('Es ist bereits ein Wert mit diesem Namen vorhanden.'));
    }
  }
}

/**
 * Callback for facet add form submit hook.
 */
function lls_facet_facets_add_submit($form, &$form_state) {

  $id = $form['id']['#value'];

  if(empty($id)) {
    db_insert(SectionMapping::DB_TABLE_TOP_SECTION)
      ->fields(array('value' => $form['facet_name']['#value']))
      ->execute();

    drupal_set_message(t('Die Facette wurde hinzugefügt'));
  }
  else {
    db_update(SectionMapping::DB_TABLE_TOP_SECTION)
      ->fields(array(
          'value' => $form['facet_name']['#value'])
      )
      ->condition('id', $id)
      ->execute();

    drupal_set_message(t('Die Facette wurde bearbeitet.'));
  }

  cache_clear_all();
  drupal_goto('/admin/config/learnline/facet-mapping');
}

/**
 * Implements the value delete form.
 */
function lls_facet_facets_delete($form, $form_state, $id) {

  $q = db_select(SectionMapping::DB_TABLE_TOP_SECTION, 'ts');
  $q->addField('ts', 'value');
  $q->condition('ts.id', $id);
  $q->range(0, 1);

  $result = $q->execute();
  $res_obj = $result->fetchObject();

  return confirm_form(
    array(
      'id' => array(
        '#type' => 'value',
        '#value' => $id,
      ),
    ),
    t('Facette löschen'),
    'admin/config/learnline/facet-mapping/values',
    t('Soll die Facette <b>%value</b> wirklich gelöscht werden?', array('%value' => $res_obj->value)),
    t('Wert löschen'),
    t('Cancel')
  );
}

/**
 * Implements delete logic for a value.
 */
function lls_facet_facets_delete_submit($form, &$form_state) {

  $id = $form_state['values']['id'];

  db_delete(SectionMapping::DB_TABLE_TOP_SECTION)
    ->condition('id', $id)
    ->execute();

  $form_state['redirect'] = '/admin/config/learnline/facet-mapping';

  drupal_set_message(t('Die Facette wurde gelöscht.'));
}
