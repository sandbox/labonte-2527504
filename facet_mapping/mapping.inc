<?php
// $Id:$

/**
 * @file
 * Module's facet display name management.
 */

use publicplan\lls\SectionMapping;

/**
 * Implements a facet display name overview.
 *
 * @return array Renderable form.
 * @throws \Exception
 */
function lls_facet_mapping() {

  drupal_set_title(t('Mapping verwalten'));

  $rows = array();

  $q = db_select(SectionMapping::DB_TABLE_VALUE, 'v');
  $q->addField('v', 'id');
  $q->addField('v', 'value');
  $q->condition('v.type', 'facet');
  $q->orderBy('v.value', 'ASC');
  $result = $q->execute();

  while($res_obj = $result->fetchObject()) {

    $operations = array(
      'edit-details' => array(
        'title' => t('Edit'),
        'href' => 'admin/config/learnline/facet-mapping/mapping/edit/' . $res_obj->id,
      ),
      'delete' => array(
        'title' => t('Delete'),
        'href' => 'admin/config/learnline/facet-mapping/mapping/delete/' . $res_obj->id,
      ),
    );

    $rows[] = array(
      $res_obj->id,
      $res_obj->value,
      get_action_theme($operations),
    );
  }

  return array(
    '#theme' => 'table',
    '#header' => array(
      t('ID'),
      t('Wert'),
      t('Operations')
    ),
    '#rows' => $rows,
    '#empty' => t('Es sind keine Anzeigenamen vorhanden.'),
  );
}

/**
 * Sets the value add page title and prepares the form.
 *
 * @return array Drupal form array.
 */
function lls_facet_mapping_add($id) {

  $type = ($id > 0) ? 'edit' : 'add';

  drupal_set_title(t(($type === 'add') ? 'Mapping hinzufügen' : 'Mapping bearbeiten'));
  return drupal_get_form('lls_facet_mapping_add_form');
}

/**
 * Value add form definition.
 *
 * @return array Renderable form.
 */
function lls_facet_mapping_add_form() {

  // Fetch ID required for edit mode
  $id = arg(6);
  $value = get_mapping_value_by_id($id);

  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Anzeigename'),
    '#default_value' => ($value !== NULL) ? $value->value : '',
    '#description' => '',
    '#required' => TRUE,
  );
  $form['facet'] = array(
    '#title' => t('Facette'),
    '#type' => 'select',
    '#default_value' => $value->top_section_id,
    '#description' => t('Wählen Sie die Facette aus, für die der Anzeigename gültig sein soll.'),
    '#options' => get_facets(),
    '#required' => TRUE,
  );
  $form['values'] = array(
    '#title' => t('Verknüpfte Werte'),
    '#type' => 'checkboxes',
    '#description' => t('Wählen Sie die Werte aus, welche unter dem Anzeigenamen zusammengefasst werden sollen.'),
    '#options' => get_mapping_values(),
    '#default_value' => get_mapping_values_checked($id),
    '#required' => TRUE,
  );
  $form['id'] = array(
    '#type' => 'hidden',
    '#default_value' => $id,
    '#required' => FALSE,
  );
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t(($id > 0) ? 'Speichern' : 'Hinzufügen'),
    '#validate' => array('lls_facet_mapping_add_validate'),
    '#submit' => array('lls_facet_mapping_add_submit'),
  );

  return $form;
}

/**
 * Callback for facet value add form validation hook.
 */
function lls_facet_mapping_add_validate($form, &$form_state) {

  if((empty($form['id']['#value']) && !empty($form['value']['#value'])) || ($form['id']['#value'] > 0 && $form['value']['#value'] !== $form['value']['#default_value'])) {

    $q = db_select(SectionMapping::DB_TABLE_VALUE, 'v');
    $q->addField('v', 'id');
    $q->condition('v.type', 'facet');
    $q->condition(db_and()->condition('v.value', $form['value']['#value']));
    $q->range(0, 1);

    $result = $q->execute();

    if($result->rowCount() > 0) {
      form_set_error('title', t('Es ist bereits ein Anzeigename mit diesem Wert vorhanden.'));
    }
  }
}

/**
 * Callback for facet add form submit hook.
 */
function lls_facet_mapping_add_submit($form, &$form_state) {

  $id = $form['id']['#value'];
  $options = $form['values']['#value'];

  if(empty($id)) {

    $id = db_insert(SectionMapping::DB_TABLE_VALUE)
      ->fields(array(
          'type' => 'facet',
          'top_section_id' => $form['facet']['#value'],
          'value' => $form['value']['#value'],
        ))
      ->execute();

    drupal_set_message(t('Der Wert wurde hinzugefügt.'));
  }
  else {

    db_update(SectionMapping::DB_TABLE_VALUE)
      ->fields(array(
          'top_section_id' => $form['facet']['#value'],
          'value' => $form['value']['#value'],
        ))
      ->condition('type', 'facet')
      ->condition(db_and()->condition('id', $id))
      ->execute();

    drupal_set_message(t('Das Mapping wurde bearbeitet.'));
  }

  // Clean old references
  db_delete(SectionMapping::DB_TABLE_CONNECT)
    ->condition('facet_id', $id)
    ->execute();

  $query = db_insert(SectionMapping::DB_TABLE_CONNECT)
    ->fields(array('facet_id', 'value_id'));

  foreach($options as $value_id) {
    $query->values(array(
      'facet_id' => $id,
      'value_id' => $value_id,
    ));
  }

  $query->execute();

  cache_clear_all();
  drupal_goto('/admin/config/learnline/facet-mapping/mapping');
}

/**
 * Implements the value delete form.
 */
function lls_facet_mapping_delete($form, $form_state, $id) {

  $q = db_select(SectionMapping::DB_TABLE_VALUE, 'v');
  $q->addField('v', 'value');
  $q->condition('v.type', 'facet');
  $q->condition(db_and()->condition('v.id', $id));
  $q->range(0, 1);

  $result = $q->execute();
  $res_obj = $result->fetchObject();

  return confirm_form(
    array(
      'id' => array(
        '#type' => 'value',
        '#value' => $id,
      ),
    ),
    t('Mapping löschen'),
    'admin/config/learnline/facet-mapping/mapping',
    t('Soll das Mapping <b>%value</b> wirklich gelöscht werden?', array('%value' => $res_obj->value)),
    t('Mapping löschen'),
    t('Cancel')
  );
}

/**
 * Implements delete logic for a value.
 */
function lls_facet_mapping_delete_submit($form, &$form_state) {

  $id = $form_state['values']['id'];

  db_delete(SectionMapping::DB_TABLE_VALUE)
    ->condition('id', $id)
    ->condition(db_and()->condition('type', 'facet'))
    ->execute();

  $form_state['redirect'] = '/admin/config/learnline/facet-mapping/mapping';

  drupal_set_message(t('Das Mapping wurde gelöscht.'));
}

/**
 * Gets the mapping value for the given ID.
 *
 * @param $id The mapping value ID.
 * @return string The mapping value.
 */
function &get_mapping_value_by_id($id) {

  $value = NULL;

  if(!empty($id)) {

    $q = db_select(SectionMapping::DB_TABLE_VALUE, 'v');
    $q->addField('v', 'top_section_id');
    $q->addField('v', 'value');
    $q->condition('v.id', $id);
    $q->condition(db_and()->condition('v.type', 'facet'));
    $result = $q->execute();

    if ($result->rowCount() > 0) {
      $value = $result->fetchObject();
    }
  }

  return $value;
}

/**
 * Get a list of available mapping values.
 *
 * @return array List of available mapping values.
 */
function &get_mapping_values() {

  $values = array();

  $q = db_select(SectionMapping::DB_TABLE_VALUE, 'v');
  $q->addField('v', 'id');
  $q->addField('v', 'value');
  $q->condition('v.type', 'value');
  $q->orderBy('v.value', 'ASC');
  $result = $q->execute();

  if($result->rowCount() > 0) {
    while($res_obj = $result->fetchObject()) {
      $values[$res_obj->id] = t($res_obj->value);
    }
  }

  return $values;
}

/**
 * Get a list of checked mapping values for the given ID.
 *
 * @return array List of checked mapping values.
 */
function &get_mapping_values_checked($id) {

  $values = array();

  if(!empty($id)) {

    $q = db_select(SectionMapping::DB_TABLE_CONNECT, 'c');
    $q->addField('c', 'value_id');
    $q->condition('c.facet_id', $id);
    $result = $q->execute();

    if ($result->rowCount() > 0) {
      while ($res_obj = $result->fetchObject()) {
        $values[] = $res_obj->value_id;
      }
    }
  }

  return $values;
}

function &get_facets() {

  $values = array();

  $q = db_select(SectionMapping::DB_TABLE_TOP_SECTION, 'ts');
  $q->addField('ts', 'id');
  $q->addField('ts', 'value');
  $q->orderBy('ts.value', 'ASC');
  $result = $q->execute();

  if($result->rowCount() > 0) {
    while($res_obj = $result->fetchObject()) {
      $values[$res_obj->id] = t($res_obj->value);
    }
  }

  return $values;
}
