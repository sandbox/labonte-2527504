<?php
// $Id:$

/**
 * @file
 * Module's facet value management.
 */

use publicplan\lls\SectionMapping;

/**
 * Implements a facet values overview.
 *
 * @return array Renderable form.
 * @throws \Exception
 */
function lls_facet_values() {

  drupal_set_title(t('Facetten Werte'));

  $rows = array();

  $q = db_select(SectionMapping::DB_TABLE_VALUE, 'v');
  $q->addField('v', 'id');
  $q->addField('v', 'value');
  $q->condition('v.type', 'value');
  $q->orderBy('v.value', 'ASC');
  $result = $q->execute();

  while($res_obj = $result->fetchObject()) {

    $operations = array(
      'edit-details' => array(
        'title' => t('Edit'),
        'href' => 'admin/config/learnline/facet-mapping/values/edit/' . $res_obj->id,
      ),
      'delete' => array(
        'title' => t('Delete'),
        'href' => 'admin/config/learnline/facet-mapping/values/delete/' . $res_obj->id,
      ),
    );

    $rows[] = array(
      $res_obj->id,
      $res_obj->value,
      get_action_theme($operations),
    );
  }

  return array(
    '#theme' => 'table',
    '#header' => array(
      t('ID'),
      t('Wert'),
      t('Operations')
    ),
    '#rows' => $rows,
    '#empty' => t('Es sind keine Werte vorhanden.'),
  );
}

/**
 * Sets the value add page title and prepares the form.
 *
 * @return array Drupal form array.
 */
function lls_facet_value_add($id) {

  $type = ($id > 0) ? 'edit' : 'add';

  drupal_set_title(t(($type === 'add') ? 'Wert hinzufügen' : 'Wert bearbeiten'));
  return drupal_get_form('lls_facet_value_add_form');
}

/**
 * Value add form definition.
 *
 * @return array Renderable form.
 */
function lls_facet_value_add_form() {

  $id = arg(6);
  $value = '';

  if(!empty($id)) {

    $q = db_select(SectionMapping::DB_TABLE_VALUE, 'v');
    $q->addField('v', 'value');
    $q->condition('v.id', $id);
    $q->condition(db_and()->condition('v.type', 'value'));
    $result = $q->execute();

    if ($result->rowCount() > 0) {
      $res_obj = $result->fetchObject();
      $value = $res_obj->value;
    }
  }

  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Wert'),
    '#default_value' => $value,
    '#description' => '',
    '#required' => TRUE,
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#default_value' => $id,
    '#required' => FALSE,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t(($id > 0) ? 'Speichern' : 'Hinzufügen'),
    '#validate' => array('lls_facet_value_add_validate'),
    '#submit' => array('lls_facet_value_add_submit'),
  );

  return $form;
}

/**
 * Callback for facet value add form validation hook.
 */
function lls_facet_value_add_validate($form, &$form_state) {

  if((empty($form['id']['#value']) && !empty($form['value']['#value'])) || ($form['id']['#value'] > 0 && $form['value']['#value'] !== $form['value']['#default_value'])) {

    $q = db_select(SectionMapping::DB_TABLE_VALUE, 'v');
    $q->addField('v', 'id');
    $q->condition('v.type', 'value');
    $q->condition(db_and()->condition('v.value', $form['value']['#value']));
    $q->range(0, 1);

    $result = $q->execute();

    if($result->rowCount() > 0) {
      form_set_error('title', t('Es ist bereits ein Wert mit diesem Namen vorhanden.'));
    }
  }
}

/**
 * Callback for facet add form submit hook.
 */
function lls_facet_value_add_submit($form, &$form_state) {

  $id = $form['id']['#value'];

  if(empty($id)) {

    db_insert(SectionMapping::DB_TABLE_VALUE)
      ->fields(array(
          'type' => 'value',
          'value' => $form['value']['#value'])
      )
      ->execute();

    drupal_set_message(t('Der Wert wurde hinzugefügt.'));
  }
  else {

    db_update(SectionMapping::DB_TABLE_VALUE)
      ->fields(array(
          'value' => $form['value']['#value'])
      )
      ->condition('type', 'value')
      ->condition(db_and()->condition('id', $id))
      ->execute();

    drupal_set_message(t('Der Wert wurde bearbeitet.'));
  }

  cache_clear_all();
  drupal_goto('/admin/config/learnline/facet-mapping/values');
}

/**
 * Implements the value delete form.
 */
function lls_facet_value_delete($form, $form_state, $id) {

  $q = db_select(SectionMapping::DB_TABLE_VALUE, 'v');
  $q->addField('v', 'value');
  $q->condition('v.type', 'value');
  $q->condition(db_and()->condition('v.id', $id));
  $q->range(0, 1);

  $result = $q->execute();
  $res_obj = $result->fetchObject();

  return confirm_form(
    array(
      'id' => array(
        '#type' => 'value',
        '#value' => $id,
      ),
    ),
    t('Wert löschen'),
    'admin/config/learnline/facet-mapping/values',
    t('Soll der Wert <b>%value</b> wirklich gelöscht werden?', array('%value' => $res_obj->value)),
    t('Wert löschen'),
    t('Cancel')
  );
}

/**
 * Implements delete logic for a value.
 */
function lls_facet_value_delete_submit($form, &$form_state) {

  $id = $form_state['values']['id'];

  db_delete(SectionMapping::DB_TABLE_VALUE)
    ->condition('id', $id)
    ->condition(db_and()->condition('type', 'value'))
    ->execute();

  $form_state['redirect'] = '/admin/config/learnline/facet-mapping/values';

  drupal_set_message(t('Der Wert wurde gelöscht.'));
}
