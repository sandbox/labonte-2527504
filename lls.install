<?php
// $id:$

/**
 * @file
 * Module install file.
 */

require_once __DIR__ . '/lls.autoload.inc';

use publicplan\lls\LearnlineSearch;
use publicplan\wss\base\SearchService;

/**
 * Implements hook_install().
 */
function lls_install() {
  // Iterate module variables:
  foreach (_lls_variables() as $name => $value) {
    // Try to read variable:
    $var = variable_get($name, "\0");
    // Check content of the variable:
    if ($var === "\0") {
      // Variable's value is neither the "\0" nor the default, so let's set it:
      variable_set($name, $value);
    }
  }
  // Clear cache tables:
  cache_clear_all();
}

/**
 * Implements hook_uninstall().
 */
function lls_uninstall() {
  // Check whether to keep the settings or not:
  if (!variable_get(SearchService::SETTING__PERSISTENT_SETTINGS, FALSE)) {
    // Remove variables:
    foreach (_lls_variables() as $name => $value) {
      variable_del($name);
    }
  }
  // Clear cache tables:
  cache_clear_all();
}

/**
 * Holds variables for (un)install routines.
 */
function _lls_variables() {
  return array(
    LearnlineSearch::SETTING__RESULTS_PER_PAGE => 10,
    LearnlineSearch::SETTING__CC_DIR => '/sites/default/files/images/cc/',
    LearnlineSearch::SETTING__REPORT_DEAD_LINK_TARGET => 'support@learnline.schulministerium.nrw.com',
    LearnlineSearch::SETTING__REPORT_DEAD_LINK_MESSAGE => array(
      'value' => _lls_report_dead_link_message_example(),
      'format' => 'full_html',
    ),
	LearnlineSearch::SETTING__REPORT_DEAD_LINK_CONFIRM_MESSAGE => array(
	  'value' => 'Möchten Sie den Link wirklich als defekt melden?',
	  'format' => 'full_html',
	),
  );
}

/**
 * Return example message to show to a user, when reporting a dead link.
 *
 * @return string
 */
function _lls_report_dead_link_message_example() {
  return <<<DEAD_LINK_MESSAGE_EXAMPLE
Vielen Dank für Ihre Mitteilung! <br/>
Wir werden die Verlinkung in Kürze prüfen.
DEAD_LINK_MESSAGE_EXAMPLE;
}
