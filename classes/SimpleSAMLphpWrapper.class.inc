<?php

namespace publicplan\lls;

/**
 * A component for performing SimpleSAML authentication actions.
 *
 * @author Roland Awemo, <awemo@publicplan.de>
 * @author Yannic Labonte, <labonte@publicplan.de>
 * @copyright (c) 2017, publicplan GmbH
 */
class SimpleSAMLphpWrapper {

  /**
   * @var SimpleSAMLphpWrapper[]
   */
  static private $authenticators = array();

  /**
   * @var SimpleSAML_Auth_Simple
   */
  private $authenticator;

  /**
   * SimpleSAMLphpWrapper constructor.
   */
  public function __construct ($samlSp) {
    $this->authenticator = new \SimpleSAML_Auth_Simple($samlSp);
  }

  /**
   * @param string $samlSp SAML2 service provider.
   * @return SimpleSAMLphpWrapper
   */
  static public function getInstance($samlSp = 'default-sp') {
    if (!isset(self::$authenticators[$samlSp])) {
      $class = get_called_class();
      self::$authenticators[$samlSp] = new $class($samlSp);
    }

    return self::$authenticators[$samlSp];
  }

  /**
   * Logs in the user with SSO on the LOGINEO Shibboleth server.
   *
   * @param array $params
   * @return array The user attributes.
   */
  public function login(array $params = []) {
    $this->authenticator->requireAuth($params);
    return $this->getAttributes();
  }

  /**
   * Logs the user out from LOGINEO.
   * @param array|null $params SimpleSAML specific parameters for the logout.
   */
  public function logout($params = null) {
    $this->authenticator->logout($params);
  }

  /**
   * Get the user attributes from the current session.
   *
   * @return array The user attributes from the current session. Empty if the user is not authenticated.
   */
  public function getAttributes() {
    return $this->authenticator->getAttributes();
  }

  /**
   * Checks if the user is authenticated.
   *
   * @return bool
   */
  public function isAuthenticated() {
    return $this->authenticator->isAuthenticated();
  }
}
