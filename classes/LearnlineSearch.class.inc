<?php
// $id:$

/**
 * @file
 * Module's main class implementation.
 */

namespace publicplan\lls;

use publicplan\wss\sodis\SodisService;

/**
 * LearnlineSearchDeactivatedException is thrown, when trying to use a
 * deactivated search service.
 */
class LearnlineSearchDeactivatedException extends \Exception {}

/**
 * class LearnlineSearch
 *
 * Perform search (and type-ahead) requests, fetch results, normalize them to
 * be compatible with each other (SODIS vs. EDMOND) and build a renderable
 * array from it.
 */
class LearnlineSearch {
  /**
   * Configuration variable names.
   */
  const SETTING__PERSISTENT_SETTINGS = 'lls_settings__persistent';
  const SETTING__RESULTS_PER_PAGE = 'lls_settings__results_per_page';
  const SETTING__CC_DIR = 'lls_settings__cc_dir';
  const SETTING__REPORT_DEAD_LINK_TARGET = 'lls_settings__report_dead_link_target';
  const SETTING__REPORT_DEAD_LINK_MESSAGE = 'lls_settings__report_dead_link_message';
  const SETTING__REPORT_DEAD_LINK_CONFIRM_MESSAGE = 'lls_settings__report_dead_link_confirm_message';

  /**
   * Recurring array keys.
   */
  const ACTIVE = 'active';
  const AVAILABLE = 'available';
  const DISPLAY_VALUE = 'display_value';
  const VALUES = 'values';
  const VALUE = 'value';
  const COUNT = 'count';
  const TITLE = 'title';
  const IS_ARRAY = 'is_array';
  const HREF = 'href';

  /**
   * Recurring array keys (espacially for parameter mapping).
   */
  const FILTER = 'filter';
  const SODIS = 'sodis';
  const EDUTAGS = 'edutags';
  const PARAM = 'parameter';
  const HIDDEN = 'hidden';
  const SQL_OR_GROUP_SEPERATOR = '|';

  /**
   * Valid input parameter names.
   * Used for parameter mapping.
   *
   * @see $parameter_mapping
   * @see __construct()
   */
  const PARAM__SEARCH = 'search';
  const PARAM__PAGE = 'page';
  const PARAM__SIZE = 'size';
  const PARAM__FROM = 'from';
  const PARAM__SORT = 'sort';
  const PARAM__ORDER = 'order';
  const PARAM__KEYWORDS = 'keywords';
  const PARAM__LANGUAGES = 'languages';
  const PARAM__CONTEXTS = 'contexts';
  const PARAM__DISCIPLINES = 'disciplines';
  const PARAM__CONTENTTYPES = 'contenttypes';
  const PARAM__PUBLISHERS = 'publishers';
  const PARAM__PROVIDERS = 'providers';
  const PARAM__COPYRIGHTS = 'copyrights';
  const PARAM__COMPETENCIES = 'competencies';
  const PARAM__EDMOND_CONTEXT = 'context';
  const PARAM__IDENTITY = 'identity';

  const SORT_BY__RELEVANCE = 'relevance';
  const SORT_BY__DATE = 'date';

  const ORDER__ASCENDING = 'asc';
  const ORDER__DESCENDING = 'desc';

  /**
   * Instance of class LearnlineSearch.
   * (Implementing a singleton pattern)
   *
   * @var \LearnlineSearch
   */
  private static $instance;

  /**
   * Array of configuration settings.
   *
   * @var array
   */
  private static $settings;

  /**
   * This module's path.
   *
   * @var string
   */
  protected $path;

  /**
   * Configured maximum count for type-ahead/auto-completion suggestions.
   *
   * @var int
   */
  protected $typeAheadCount;

  /**
   * Decoded JSON result of a search request to SODIS.
   *
   * @var array
   */
  protected $result = array(
    'error' => array(),
  );

  /**
   * Formatted result data as renderable array.
   *
   * @var array
   */
  protected $formatted = array();

  /**
   * Simple list of available facets for internal use
   * @see LearnlineSearch::parseSodisFacets()
   *
   * @var type
   */
  private $availableFacets = array();

  /**
   * Parameter mapping.
   *
   * @var array
   */
  protected $parameterMapping;

  public $test = array();

  /**
   * Call LearnlineSearch::getInstance() instead of using the constructor.
   */
  protected function __construct() {
    $this->parameterMapping = array(
      self::PARAM__SEARCH => array(
        self::TITLE => t('Search term'),
        self::SODIS => array(
          self::PARAM => 'q',
        ),
      ),
//      self::PARAM__EDMOND => array(
//        self::TITLE => t('EDMOND identity'),
//        self::IS_ARRAY => TRUE,
//        self::EDMOND => array(
//          self::PARAM => '%identity',
//        ),
//      ),
      self::PARAM__PAGE => array(
      ),
      self::PARAM__SIZE => array(
        self::SODIS => TRUE,
      ),
      self::PARAM__FROM => array(
        self::SODIS => TRUE,
      ),
      self::PARAM__SORT => array(
        self::TITLE => t('Sorting criteria'),
        self::SODIS => TRUE,
      ),
      self::PARAM__ORDER => array(
        self::TITLE => t('Sort order'),
        self::SODIS => TRUE,
      ),
      self::PARAM__CONTEXTS => array(
        self::TITLE => t('Resource context'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'context',
          'll3:context'
        ),
      ),
      self::PARAM__DISCIPLINES => array(
        self::TITLE => t('Disciplines'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'discipline',
          array('lom:classificationDisciplineId' => 'lom:classificationDisciplineEntry'),
          array('ll3:classificationDisciplineId' => 'll3:classificationDisciplineEntry'),
        ),
      ),
      self::PARAM__CONTENTTYPES => array(
        self::TITLE => t('Media types'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'learningResourceType',
          'll3:learningResourceType',
//          'lom:educational_learningResourceType',
//          'lom:technical_format',
        ),
      ),
      self::PARAM__PUBLISHERS => array(
        self::TITLE => t('Publishers'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'publisher',
          'lom:publisher'
        ),
      ),
      self::PARAM__COPYRIGHTS => array(
        self::TITLE => t('Copyright'),
        self::IS_ARRAY => TRUE,
        self::FILTER => function ($input) {
          return strtoupper($input);
        },
        self::SODIS => array(
          self::PARAM => 'copyright',
          'lom:rights',
        ),
      ),
      self::PARAM__LANGUAGES => array(
        self::TITLE => t('Languages'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'language',
          'lom:general_language'
        ),
      ),
      self::PARAM__KEYWORDS => array(
        self::TITLE => t('Keywords'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'keyword',
          'lom:general_keyword'
        ),
      ),
      self::PARAM__PROVIDERS => array(
        self::TITLE => t('Providers'),
        self::HIDDEN => TRUE,
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'provider',
//          'lom:provider'
        ),
      ),
      self::PARAM__COMPETENCIES => array(
        self::TITLE => t('Competencies'),
        self::HIDDEN => TRUE,
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'competency',
        ),
      ),
    );
  }

  /**
   * Implements a replacement for the constructor, considering the singleton
   * pattern.
   *
   * @return \LearnlineSearch Returns itself.
   */
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new LearnlineSearch();
    }

    foreach (array(
        self::SETTING__RESULTS_PER_PAGE => 10,
        self::SETTING__REPORT_DEAD_LINK_MESSAGE => null,
        self::SETTING__CC_DIR => '/sites/default/files/images/cc/',
    ) as $setting => $default_value) {
      if ((self::$settings[$setting] = variable_get($setting, $default_value)) === null) {
        throw new \RuntimeException(
            t("%class is missing setting '%setting'.", array(
                '%class' => get_called_class(),
                '%setting' => $setting)));
      }
    }

    return self::$instance;
  }

  public static function getSetting($setting_variable_name) {
    self::getInstance();
    if (!isset(self::$settings[$setting_variable_name])) {
      throw new \Exception(t("%class has no setting named '%setting'.", array(
          '%class' => get_called_class(),
          '%setting' => $setting_variable_name)));
    }
    return self::$settings[$setting_variable_name];
  }

  /**
   * Stage request to the SODIS search API and retrieve normalized results.
   *
   * @param   array $parameters Search parameter set.
   * @return  \LearnlineSearch   Returns itself for method chaining.
   * @throws  RuntimeException
   */
//  public function search($parameters) {
//    try {
//      $this->result[self::SODIS] = SearchServiceSodis::getInstance()
//          ->performSearch($this)->getResult();
//    } //try
//    catch (LearnlineSearchDeactivatedException $e) {
//      throw $e;
//    } //catch
//    catch (Exception $e) {
//      while ($e) {
//        printf("[%s:%d]<br/>\n\t%s<br/><pre>%s</pre>\n",
//            $e->getFile(), $e->getLine(), $e->getMessage(), $e->getTraceAsString());
//        $e = $e->getPrevious();
//      } //while
//    } //catch
//
//    try {
//      $this->result[self::EDMOND] = SearchServiceEdmond::getInstance()
//          ->performSearch($this)->getResult();
//    } //try
//    catch (LearnlineSearchDeactivatedException $e) {
//      // Do nothing, just omit further operation concerning EDMOND search.
//    } //catch
//    catch (Exception $e) {
//      while ($e) {
//        printf("[%s:%d]<br/>\n\t%s<br/><pre>%s</pre>\n",
//            $e->getFile(), $e->getLine(), $e->getMessage(), $e->getTraceAsString());
//        $e = $e->getPrevious();
//      } //while
//    } //catch
//
//    return $this;
//  } //function

  /**
   * Format search results and return them as renderable array.
   *
   * Should only be called AFTER at least one searchXYZ()-method had be invoked!
   *
   * @return array      A renderable array for the content region.
   * @throws Exception
   */
//  public function formatResults() {
//    if (!isset($this->result['sodis'])) {
//      throw new Exception('Stage a request before trying to format its results.');
//    } //if
//
//    $query_without_page = drupal_get_query_parameters(null, array('page'));
//
//    // Build renderable array:
//    $this->formatted = array(
//      'content' => array(
//        '#theme' => 'learnline_search_result',
//        '#searchword' => $query_without_page[self::PARAM__SEARCH],
//        '#results' => array(),
//        '#total' => (int) $this->result['sodis']['hits']['total'],
//        '#per_page' => (int) variable_get(LearnlineSearch::SETTING__RESULTS_PER_PAGE),
//        '#page_no' => isset($_GET[self::PARAM__PAGE]) ? (int) $_GET[self::PARAM__PAGE] : 1,
//        '#pagination_base' => $this->queryToString($query_without_page) . '&page=',
//        '#error' => $this->result['error'] ?: null,
//        '#edmond_teaser' => SearchServiceEdmond::getInstance()->getFormattedContent('teaser'),
//      ),
//      'sidebar_second' => array(
//        '#theme' => 'learnline_search_result_sidebar',
//        '#facets' => array(),
//        '#query_url' => $this->queryToString($query_without_page) . '&',
//      ),
//    );
//
//    // Iterate results:
//    foreach ($this->result['sodis']['hits']['hits'] as $no => $hit) {
//      if (isset($this->formatted['content']['#results'][$hit['_id']])) {
//        continue;
//      } //if
//
//      $rating = fivestar_get_votes('resource', $hit['_id']);
//      $rating_form = drupal_get_form('fivestar_custom_widget', $rating, array(
//          'entity_id' => $hit['_id'],
//          'entity' => $hit['_id'], // <-- Workaround for a bug in fivestar module.
//          'entity_type' => 'resource',
//          'content_type' => 'resource', // <-- Workaround for a bug in fivestar module.
//          'stars' => 5,
//          'autosubmit' => TRUE,
//          'allow_clear' => TRUE,
//          'required' => FALSE,
//          'tag' => 'vote',
//          'text' => 'none',
//          'style' => 'smart',
//          'widget' => array(
//            'name' => 'oxygen',
//            'css' => drupal_get_path('module', 'fivestar') . '/widgets/oxygen/oxygen.css',
//          ),
//      ));
//
//      $cc_short = array();
//      // Build renderable array from result:
//      $this->formatted['content']['#results'][$hit['_id']] = array(
//        '#id' => $hit['_id'],
//        '#title' => $hit['_source']['lom:general_title'],
//        '#href' => '/sodis/resource/' . $hit['_id'],
//        '#filesize' => '',
//        '#publisher' =>
//            isset($hit['_source']['lom:publisher']) &&
//            !empty($hit['_source']['lom:publisher'])
//            ? implode(', ', $hit['_source']['lom:publisher'])
//            : '',
//        '#provider' =>
//            isset($hit['_source']['lom:provider']) &&
//            !empty($hit['_source']['lom:provider'])
//            ? implode(', ', $hit['_source']['lom:provider'])
//            : '',
//        '#contenttype' =>
//            isset($hit['_source']['lom:technical_format'])
//            ? $hit['_source']['lom:technical_format']
//            : '',
//        '#resourcetype' =>
//            isset($hit['_source']['ll3:learningResourceType']) &&
//            !empty($hit['_source']['ll3:learningResourceType'])
//            ? implode(', ', $hit['_source']['ll3:learningResourceType'])
//            : '',
//        '#copyright' =>
//            isset($hit['_source']['lom:rights']) &&
//            !empty($hit['_source']['lom:rights']) &&
//            preg_match('/^CC:(.+)$/', $hit['_source']['lom:rights'], $cc_short)
//            ? $cc_short[1]
//            : '',
//        '#description' => isset($hit['_source']['lom:general_description'])
//            ? $hit['_source']['lom:general_description']
//            : '[ ' . t('No description available.') . ' ]',
//        '#rating' => $rating_form,
//        '#facets' => array(),
//      );
//
//      // Add thumbnail if applicable:
//      if (isset($hit['_source']['lom:relation_hasThumbnail']) &&
//          is_array($hit['_source']['lom:relation_hasThumbnail'])) {
//        $this->formatted['content']['#results'][$hit['_id']]['#thumbnail'] = array(
//          '#theme' => 'learnline_search_result_thumbnail',
//          '#images' => $hit['_source']['lom:relation_hasThumbnail'],
//        );
//      }
//
//      $this->parseSodisFacets($hit, $query_without_page);
////      foreach ($this->parseSodisFacets($hit, $query_without_page) as $facet_name => $facet) {
////        if (!isset($this->formatted['sidebar_second']['#facets'][$facet_name])) {
////          $this->formatted['sidebar_second']['#facets'][$facet_name] = $facet;
////        } //if
////        else {
////          foreach ($facet[self::VALUES] as $facet_value) {
////            if (isset($this->formatted['sidebar_second']['#facets'][$facet_name][self::VALUES][$facet_value[self::DISPLAY_VALUE]])) {
////              $this->formatted['sidebar_second']['#facets'][$facet_name][self::VALUES][$facet_value[self::DISPLAY_VALUE]][self::COUNT] +=
////                  $facet_value[self::COUNT];
////
////              if (isset($facet_value[self::ACTIVE])) {
////                $this->formatted['sidebar_second']['#facets'][$facet_name][self::VALUES][$facet_value[self::DISPLAY_VALUE]][self::ACTIVE] =
////                    $facet_value[self::ACTIVE];
////              } //if
////            } //if
////            else {
////              $this->formatted['sidebar_second']['#facets'][$facet_name][self::VALUES][$facet_value[self::DISPLAY_VALUE]] = $facet_value;
//////              var_dump($facet_value, $this->formatted['sidebar_second']['#facets']);
////            } //else
////          } //foreach
////        } //else
////      } //foreach
//    } //foreach
//
//    $this->formatted['sidebar_second']['#facets'] = SearchServiceSodis::getInstance()->getFormattedFacets();
////var_dump($this->formatted['sidebar_second']['#facets']);die;
//    // Sort facets by the circumstance, if they are active or not and then
//    // by natural sort of their display names:
//    array_walk($this->formatted['sidebar_second']['#facets'], function (&$facet) {
//      usort($facet, function ($f1, $f2) {
//        if (isset($f1[LearnlineSearch::ACTIVE]) &&
//            !isset($f2[LearnlineSearch::ACTIVE])) {
//          return -1;
//        }
//        elseif (isset($f2[LearnlineSearch::ACTIVE]) &&
//            !isset($f1[LearnlineSearch::ACTIVE])) {
//          return 1;
//        }
//        elseif ($f1[LearnlineSearch::COUNT] !== $f2[LearnlineSearch::COUNT]) {
//          return $f2[LearnlineSearch::COUNT] - $f1[LearnlineSearch::COUNT];
//        }
//        else return strnatcmp(
//            $f1[LearnlineSearch::DISPLAY_VALUE],
//            $f2[LearnlineSearch::DISPLAY_VALUE]);
//      });
//    });
//
//    $this->formatted['sidebar_second']['#legal_notice'] = variable_get(
//        LearnlineSearch::SETTING__LEGAL_NOTICE);
//    $this->formatted['sidebar_second']['#legal_notice']['heading'] = variable_get(
//        LearnlineSearch::SETTING__LEGAL_NOTICE_HEADING);
//
//    return $this;
//  } //function

  /**
   * Returns a renderable array for the search result's content region.
   *
   * Should only be called AFTER at least one searchXYZ()-method had be invoked!
   *
   * @return array      Renderable array for the content region.
   * @throws Exception
   */
//  public function getFormattedContent() {
//    if (empty($this->formatted) || !isset($this->formatted['content'])) {
//      return array();
////      throw new Exception('Stage a request and format its results before trying to format its results.');
//    } //if
//
//    return $this->formatted['content'];
//  } //function

  /**
   * Returns a renderable array for the search result's sidebar.
   *
   * Should only be called AFTER at least one searchXYZ()-method had be invoked!
   *
   * @return array      Renderable array for the sidebar_second region.
   * @throws Exception
   */
//  public function getFormattedSidebar() {
//    if (empty($this->formatted) || !isset($this->formatted['sidebar_second'])) {
//      return array();
////      throw new Exception('Stage a request and format its results before trying to format its results.');
//    } //if
//
//    // Add inline Javascript to the footer:
//    $js_inline_opts = array(
//      'type' => 'inline',
//      'scope' => 'footer',
//    );
//    drupal_add_js("new learnline.PageOverlay('#rechtshinweis_show', '#rechtshinweis_text');",
//        $js_inline_opts);
//
//    return $this->formatted['sidebar_second'];
//  } //function

  /**
   * Converts a query array into a query string.
   *
   * @param array   $queryArray Query array as retrieved from drupal_get_query_array().
   * @return string             The appropriate query string starting with '?'.
   */
  public function queryToString($queryArray) {
    $query = array();
    foreach ($queryArray as $key => $value) {
      if ($key === 'q') {
        // We will remove the q parameter, because it is reserved by Drupal to
        // keep the current request path.
        continue;
      }

      if (is_array($value)) {
        foreach ($value as $arr_value) {
          $query[] = $key . '[]=' . urlencode($arr_value);
        }
      }
      else {
        $query[] = $key . '=' . urlencode($value);
      }
    }

    return '?' . implode('&', $query);
  }

  /**
   * Parse SODIS facets.
   * @see LearnlineSearch->search()
   *
   * @param array $hit
   * @param array $query
   * @return array
   */
//  public function parseSodisFacets($hit, $query) {
//    $facets = array();
//    foreach ($this->parameter_mapping as $facet_name => $map) {
//      if (!isset($this->available_facets[$facet_name])) {
//        $this->available_facets[$facet_name] = array();
//      } //if
//
//      // Continue immediately if there's no mapping:
//      if (!isset($map[self::SODIS][0])
//          || !isset($map[self::TITLE])
//          || empty($map[self::TITLE])) {
//        continue;
//      } //if
//
//      $facets[$facet_name] = array(
//        self::TITLE => $map[self::TITLE],
//        self::VALUES => array(),
//      );
//
//      // Iterate mappings:
//      foreach ($map[self::SODIS] as $map_key => $mapping) {
//        // Continue if there's nothing to map:
//        if (!is_numeric($map_key)
//            || (is_string($mapping) && !isset($hit['_source'][$mapping]))
//            || (is_string($mapping) && empty($hit['_source'][$mapping]))) {
//          continue;
//        } //if
//
//        $available = $this->parseSodisFacetAvailability($mapping, $hit);
//
//        if (!$available) {
//          continue;
//        } //if
//
//        foreach ($available as $available_facet) {
//          // Assign facet to the corresponding result:
//          $this->formatted['content']['#results'][$hit['_id']]['#facets'][$facet_name]
//              [ $available_facet[self::VALUE] ] = $available_facet[self::DISPLAY_VALUE];
//
////          if (isset($query[$facet_name]) &&
////              is_int($idx = array_search($available_facet[self::DISPLAY_VALUE], $query[$facet_name]))) {
////            $query_base = $query;
////            unset($query_base[$facet_name][$idx]);
////            $available_facet[self::ACTIVE] = $this->queryToString($query_base);
////          } //if
////
////          $this->available_facets[$facet_name][$available_facet[self::DISPLAY_VALUE]] = TRUE;
////          $facets[$facet_name][self::VALUES][$available_facet[self::DISPLAY_VALUE]] = $available_facet;
//        } //foreach
//      } //foreach
//    } //foreach
////    return $facets;
//  } //function

  /**
   * Parses all facets delivered with the given hit.
   * @see LearnlineSearch->parseSodisFacets()
   *
   * @param string|array $mapping
   * @return array
   */
//  public function parseSodisFacetAvailability($mapping, &$hit) {
//    $display_value_key = '';
//    $value_key = '';
//
//    $mapper = function (&$value, &$display_value = '') {
//      return array(
//        LearnlineSearch::DISPLAY_VALUE => $display_value ?: $value,
//        LearnlineSearch::VALUE => $value,
//        LearnlineSearch::COUNT => 1,
//      );
//    };
//
//    if (is_string($mapping) && isset($hit['_source'][$mapping])
//        && is_string($hit['_source'][$mapping])) {
//      return array_map($mapper, array($hit['_source'][$mapping]));
//    }
//    elseif (is_string($mapping) && isset($hit['_source'][$mapping])
//        && is_array($hit['_source'][$mapping])) {
//      $display_value_key = $value_key = $mapping;
//      return array_map($mapper, $hit['_source'][$value_key]);
//    } //if
//    elseif (is_array($mapping)) {
////      $value_key = key($mapping);
////      $display_value_key = reset($mapping);
////
////      if (isset($hit['_source'][$value_key])
////          && isset($hit['_source'][$display_value_key])
////          && is_array($hit['_source'][$value_key])
////          && is_array($hit['_source'][$display_value_key])) {
////        return array_map($mapper, $hit['_source'][$value_key],
////            $hit['_source'][$display_value_key]);
////      } //if
//      foreach ($mapping as $value_key => $display_value_key) {
//        if (isset($hit['_source'][$value_key])
//            && isset($hit['_source'][$display_value_key])
//            && is_array($hit['_source'][$value_key])
//            && is_array($hit['_source'][$display_value_key])) {
//          return array_map($mapper, $hit['_source'][$value_key],
//              $hit['_source'][$display_value_key]);
//        } //if
//      } //foreach
//    } //elseif
//    return FALSE;
//  } //function

  /**
   * Returns the human readable title for a facet.
   *
   * @param string $filter_name
   * @return string
   * @throws Exception
   */
  public function getFilterTitle($filter_name) {
    if (!isset($this->parameterMapping[$filter_name])) {
      throw new \Exception("Unknown facet '$filter_name'.");
    }
    elseif (!isset($this->parameterMapping[$filter_name][self::TITLE])) {
      throw new \Exception("Unknown title for facet '$filter_name'.");
    }
    return $this->parameterMapping[$filter_name][self::TITLE];
  }

  /**
   * Filters a facet's value according to its parameter mapping configuration.
   *
   * @param string $facet_name
   * @param string $input
   * @return string
   */
  public function facetFilter($facet_name, $input) {
    if (isset($this->parameterMapping[$facet_name]) &&
        isset($this->parameterMapping[$facet_name][self::FILTER]) &&
        is_callable($this->parameterMapping[$facet_name][self::FILTER])) {

      return call_user_func(
          $this->parameterMapping[$facet_name][self::FILTER],
          $input);
    }
    return $input;
  }

  /**
   * Check whether a facet can receive multiple arguments or not.
   *
   * @param string $facet_name
   *   Facet name
   *
   * @return bool
   *   TRUE if the facet can/should be used as array.
   */
  public function isMultiFacet($facet_name) {
    return isset($this->parameterMapping[$facet_name][self::IS_ARRAY])
        && $this->parameterMapping[$facet_name][self::IS_ARRAY];
  }

  public static function getCacheExpirationTime() {
    return time() + 1800;
  }
}
