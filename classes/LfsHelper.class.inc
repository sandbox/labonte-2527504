<?php
// $id:$

/**
 * @file
 * wss_edmond module helper class.
 */

namespace publicplan\lls;

use publicplan\wss\lfs\Service;

class LfsHelper extends WssHelper {

  /**
   * Helper class instance for backend configurable mappings.
   *
   * @var \publicplan\lls\SectionMapping
   */
  private static $LfsSectionMapping;

  /**
   * Radius in km to use for nearby search.
   *
   * @var int
   */
  protected $nearbyRaduis = 50;

  /**
   * Address or coordinates to use for nearby search.
   *
   * @var string|null
   */
  protected $nearbyAddress = NULL;

  /**
   * A list of query parameter name to display name mappings.
   *
   * @var array
   */
  static $paramNameMapping = array(
    Service::PARAM__CONTEXTS => 'Bildungsbereiche',
    Service::PARAM__DISCIPLINES => 'Fach- und Sachgebiete',
    Service::PARAM__TRAEGER => 'Träger',
    Service::PARAM__VERANSTALTER => 'Veranstalter',
    Service::PARAM__VERANSTALTUNGSFORM => 'Veranstaltungsform',
    Service::PARAM__KEYWORDS => 'Schlüsselwörter',
    Service::PARAM__LANGUAGES => 'Sprachen',
    Service::PARAM__CONTENTTYPES => 'Medientypen',
    Service::PARAM__PUBLISHERS => 'Herausgeber',
    Service::PARAM__COPYRIGHTS => 'Urheberrecht',
  );

  /**
   * Initialize parameter mapping array.
   */
  public function initializeParameterMappings() {
    $this->parameterMappings = array(
      LearnlineSearch::PARAM__PAGE => array(
        self::MAP_TYPE => self::TYPE__CALLBACK | self::TYPE__NO_FILTER,
        self::MAP_CALLBACK => array($this, 'mapPage'),
        self::MAP_DEFAULT => 1,
      ),
      'ZeitraumVon' => array(
        self::MAP_TITLE => 'Datum von',
        self::MAP_TYPE => self::TYPE__DEFAULT,
        self::MAP_PARAMETER => 'ZeitraumVon',
      ),
      'ZeitraumBis' => array(
        self::MAP_TITLE => 'Datum bis',
        self::MAP_TYPE => self::TYPE__DEFAULT,
        self::MAP_PARAMETER => 'ZeitraumBis',
      ),
      'AufAnfrage' => array(
        self::MAP_TITLE => 'Auf Anfrage',
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => 'AufAnfrage',
      ),
      'Traeger' => array(
        self::MAP_TITLE => 'Träger',
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => 'Traeger',
      ),
      'Veranstalter' => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => 'Veranstalter',
      ),
      'Veranstaltungsort' => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => 'Veranstaltungsort',
      ),
      'Veranstaltungsform' => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => 'Veranstaltungsform',
      ),
      'contexts' => array(
        self::MAP_TITLE => 'Bildungsbereich',
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => 'Schulformen',
      ),
      'disciplines' => array(
        self::MAP_TITLE => 'Fach',
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => 'Berufsfelder',
      ),
      'Kategorie' => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => 'Kategorie',
      ),
//      'umkreis' => array(
//        self::MAP_TYPE => self::TYPE__DEFAULT,
//        self::MAP_PARAMETER => 'umkreis',
//      ),
//      'adresse' => array(
//        self::MAP_TYPE => self::TYPE__DEFAULT,
//        self::MAP_PARAMETER => 'adresse',
//      ),
    );
  }

  /**
   * Set the nearby search radius in km.
   *
   * @param int $radius
   *   A number, defining the nearby search radius in km.
   *
   * @return \publicplan\lls\LfsHelper
   *   Returns itself.
   */
  public function setNearbySearchRadius($radius) {
    $this->nearbyRaduis = abs((int)$radius);
    return $this;
  }

  /**
   * Set the nearby search address.
   *
   * @param string $address
   *   City, postcode, postal address or something like that.
   *
   * @return \publicplan\lls\LfsHelper
   *   Returns itself.
   */
  public function setNearbySearchAddress($address) {
    $this->nearbyAddress = $address;
    return $this;
  }

  /**
   * Unset the nearby search address (deactivates the nearby search).
   *
   * @return \publicplan\lls\LfsHelper
   *   Returns itself.
   */
  public function unsetNearbySearchAddress() {
    $this->nearbyAddress = NULL;
    return $this;
  }

  /**
   * Build search URL based on a given search term.
   *
   * @param string $search_term
   *   Search term.
   *
   * @return string
   *   Search URL.
   */
  public function buildSearchUrl($search_term) {
    $search_url = Service::getSetting(Service::SETTING__BASE_URI);
    $search_url .= (strstr($search_url, '?') ? '&' : '?') . 'term=' . urlencode($search_term);

    return $search_url;
  }

  /**
   * Map learnline parameters to match the LfS parameter naming.
   *
   * @param array $learnline_parameters
   *   Input parameters.
   *
   * @return array
   *   LfS request parameters.
   */
  public function mapParameters($learnline_parameters, $service) {
    if (!isset($learnline_parameters['page'])) {
      $learnline_parameters['page'] = 1;
    }

    parent::mapParameters($learnline_parameters, $service);

    // LfS specific mapping (configurable in the Drupal backend)
    foreach ($learnline_parameters as $input_param => $value) {
      // Determine output and display name for given input query parameter
      $param_display_name = $this->queryParamToName($input_param);
      $param_output_name = _lls_var_get($this->parameterMappings, $input_param . '|' . self::MAP_PARAMETER);
      // Parameter mapping if available
      if (!empty($param_display_name)) {
        if (!isset(self::$LfsSectionMapping)) {
          self::$LfsSectionMapping = new SectionMapping();
        }

        $mapped_values = self::$LfsSectionMapping->getMappedValues($param_display_name, $value);
        foreach ($mapped_values as $unmapped_value => $mapped_value) {
          if (empty($mapped_value)) {
            continue;
          }
          $mapped_terms = implode('|', $mapped_value);
          if (empty($this->outputParameters[$param_output_name])) {
            $this->outputParameters[$param_output_name] = array();
          }
          $original_value = array_search($unmapped_value, $this->outputParameters[$param_output_name]);
          if ($original_value !== FALSE) {
            unset($this->outputParameters[$param_output_name][$original_value]);
          }
          $this->outputParameters[$param_output_name] = array_merge(
            $this->outputParameters[$param_output_name],
            array($mapped_terms)
          );
        }
      }
    }

    // Append nearby search params if applicable
    if (is_string($this->nearbyAddress)) {
      $this->outputParameters['umkreis'] = $this->nearbyRaduis;
      $this->outputParameters['adresse'] = $this->nearbyAddress;
    }

    return $this->outputParameters;
  }

  /**
   * @param string $param
   *  The query parameter name.
   *
   * @return mixed
   *  Returns the query parameter display name or NULL if no mapping available.
   */
  protected function queryParamToName($param) {
    if (isset(self::$paramNameMapping[$param])) {
      return self::$paramNameMapping[$param];
    }

    return NULL;
  }

  /**
   * Callback function.
   *
   * This function maps a page number to the appropriate 'limit' and 'offset'
   * values as desired from the webservice.
   *
   * @param int $page_num
   * @return \publicplan\lls\LfsHelper
   * @throws \Exception
   */
  public function mapPage($page_num) {
    $limit = LearnlineSearch::getSetting(LearnlineSearch::SETTING__RESULTS_PER_PAGE);
//    $offset = ($page_num - 1) * $limit;
    // The Neofonie LeFo API multiplies the offset param by the passed limit!
    $offset = $page_num - 1;

    $this->outputParameters['limit'] = $limit;
    $this->outputParameters['offset'] = $offset;

    return $this;
  }

  /**
   * Prepare list of available filters for the template.
   *
   * @param array $filters Filter array as returned from the response object.
   * @param array $query_parameters Current query parameters.
   * @return array Prepared filter array.
   */
  public function prepareAvailableFilters($filters, $query_parameters) {
    $output = array();
    if (empty($filters)) {
      return $output;
    }

    foreach ($filters as $filter => $values) {
      $filter_array = array(
        'title' => $this->getFilterTitle($filter),
        'children' => array(),
      );
      foreach ($values as $value => $count) {
        if (isset($query_parameters[$filter]) && $query_parameters[$filter] === $value) {
          // Skip values for which the filter is already set.
          continue;
        }
        $filter_array['children'][] = array(
          'value' => $value,
          'count' => $count,
          'query' => $this->queryToString($this->queryAdd($query_parameters, $filter, $value)),
        );
      }
      if (!empty($filter_array['children'])) {
        $output[$filter] = $filter_array;
      }
    }

    return $output;
  }

  /**
   * Get an array of active facets based on the given query parameter array.
   *
   * The array being returned is a nested structure with each parameter value,
   * as element, containing the query parameter, the parameter value itself,
   * the human readable parameter name and a an array structure for building
   * a query without the specific parameter value.
   *
   * @param array $query_parameters
   *   Query parameter array.
   *
   * @return array
   *   Nested array structure as described above.
   */
  public function getActiveFilters($query_parameters) {
    $active_filters = array();
    foreach ($query_parameters as $query_param => $query_value) {
      if (empty($query_value) || !isset($this->parameterMappings[$query_param]) ||
        $this->isTypeNoFilter($this->parameterMappings[$query_param][self::MAP_TYPE])) {
        continue;
      }
      if (in_array($query_param, array('ZeitraumVon', 'ZeitraumBis'))) {
        $word = strtolower(substr($query_param, -3));
        $active_filter = array(
          'param' => $query_param,
          'value' => "Im Zeitraum {$word}: " . $query_value,
          'title' => $this->getFilterTitle($query_param),
          'query' => $this->queryToString($this->queryRemove($query_parameters, $query_param, $query_value)),
        );
      }
      elseif (is_array($query_value)) {
        foreach ($query_value as $value) {
          if (empty($value)) {
            continue;
          }
          $active_filters[] = $this->getActiveFilter($query_parameters, $query_param, $value);
        }
        continue;
      } else {
        $active_filter = $this->getActiveFilter($query_parameters, $query_param, $query_value);
      }
      $active_filters[] = $active_filter;
    }

    return $active_filters;
  }

  /**
   * Get/Build one element for the array structure returned by getActiveFacets().
   *
   * @param array $query
   *   Query parameter array.
   * @param string $query_param
   *   A query parameter name.
   * @param string $query_value
   *   A query parameter value.
   *
   * @return array
   *   Array containing all necessary information for the given query parameter
   *   value.
   *
   * @throws \Exception
   *   Throws an exception on misconfiguration.
   */
  private function getActiveFilter($query, $query_param, $query_value) {
    return array(
      'param' => $query_param,
      'value' => $query_value,
      'title' => $this->getFilterTitle($query_param),
      'query' => $this->queryToString($this->queryRemove($query, $query_param, $query_value)),
    );
  }

  /**
   * Override queryAdd() method to ensure the correct primary_service parameter.
   */
  public function queryAdd($query, $param, $value) {
    $query['primary_service'] = 'lfs';
    return parent::queryAdd($query, $param, $value);
  }

  /**
   * Override queryRemove() method to ensure the correct primary_service parameter.
   */
  public function queryRemove($query, $param, $value = NULL) {
    $query['primary_service'] = 'lfs';
    return parent::queryRemove($query, $param, $value);
  }

  /**
   * Return url string for given filter.
   *
   * @param string $param
   * @param string $value
   * @return string
   */
  public function linkFilter($param, $value) {
    $query = drupal_get_query_parameters();
    $query['primary_service'] = 'lfs';

    // Only one value of discipline is allowed
    if($param === 'disciplines') {
      $query[$param] = array();
    }

    return '?' . $this->queryToString($this->queryAdd($query, $param, $value));
  }

  protected function createReversedFilterMappings() {

  }
}
