<?php

namespace publicplan\lls;

class SectionMapping {

  /**
   * The term parameter name for use in arrays.
   */
  const PARAM_TERM = 'term';

  /**
   * The filters parameter name for use in arrays.
   */
  const PARAM_FILTERS = 'filters';

  /**
   * The mapping parameter name for use in arrays.
   */
  const PARAM_MAPPING = 'mapping';

  /**
   * The database table name for mapping values.
   */
  const DB_TABLE_VALUE = 'mapping_value';

  /**
   * The database table name for mapping connections.
   */
  const DB_TABLE_CONNECT = 'mapping_connect';

  /**
   * The database table name for mapping top sections.
   */
  const DB_TABLE_TOP_SECTION = 'mapping_top_section';

  const CACHE_MAPPING_KEY = 'llnrw_mapping_cache';

  /**
   * Mapping array for LFS search.
   * Use self::$mappingValues for assignment.
   *
   * @see SectionMapping::initMappingValues()
   *
   * @var array
   */
  protected $filterMapping;

  /**
   * The list of available mapping values.
   *
   * @see SectionMapping::initMappingValues()
   *
   * @var array
   */
  protected $mappingValues = array();

  /**
   * LFSMapping constructor.
   */
  public function __construct() {
    $this->initMappingValues();
  }

  /**
   * Gets the mapped values for the given $key and $value.
   *
   * @param string $key
   *  The top term.
   * @param mixed $input_value
   *  The inner term.
   *
   * @return array
   *  Returns an array with the mapped strings or an empty array if is no mapping available.
   */
  public function getMappedValues($key, $input_value) {
    $input_values = is_array($input_value) ? $input_value : array($input_value);
    $mapped_values = array();

    // Build array with filter values
    foreach ($this->filterMapping as $filter) {
      if ($filter[self::PARAM_TERM] !== $key) {
        continue;
      }

      foreach ($filter[self::PARAM_FILTERS] as $filter_mapping) {
        $unmapped_value = array_search($filter_mapping[self::PARAM_TERM], $input_values);
        if ($unmapped_value !== FALSE) {
          $single_mapping = $filter_mapping[self::PARAM_MAPPING];

          $mapped_values[$input_values[$unmapped_value]] = array_merge(
            array($input_values[$unmapped_value]),
            $this->getMappedStrings($single_mapping)
          );
        }
      }

      break;
    }

    // Mapped values to string
    return $mapped_values;
  }

  /**
   * Returns the mapped strings for the given mapped value array.
   *
   * @param array $mapped_values
   *  An array with mapped values.
   *
   * @return array
   *  Returns an array with the mapped strings.
   */
  protected function getMappedStrings($mapped_values) {
    $mapped_value_names = array();
    foreach($mapped_values as $id) {
      if(isset($this->mappingValues[$id]) === true) {
        $mapped_value_names[] = $this->mappingValues[$id];
      }
    }

    return $mapped_value_names;
  }

  /**
   * Build the mapping arrays.
   *
   * @see SectionMapping::$mappingValues
   */
  protected function initMappingValues() {

    // Get mapping table from cache
    // Build mapping array and write it to cache if cache not exists
    if (false && $cached = cache_get(self::CACHE_MAPPING_KEY, 'cache'))  {
      $this->filterMapping = $cached->data;
    }
    else {
      $q = db_select(SectionMapping::DB_TABLE_VALUE, 'mv');
      $q->addField('mv', 'id');
      $q->addField('mv', 'type');
      $q->addField('mv', 'value');
      $q->condition('mv.type', 'value');
      $result = $q->execute();

      while($res_obj = $result->fetchObject()) {
        $this->mappingValues[$res_obj->id] = $res_obj->value;
      }

      $q = db_select(SectionMapping::DB_TABLE_CONNECT, 'mc');
      $q->join(SectionMapping::DB_TABLE_VALUE, 'mvv', 'mvv.id = mc.value_id');
      $q->join(SectionMapping::DB_TABLE_VALUE, 'mvf', 'mvf.id = mc.facet_id');
      $q->leftJoin(SectionMapping::DB_TABLE_TOP_SECTION, 'mts', 'mts.id = mvf.top_section_id');
      $q->addField('mvv', 'id', 'mvv_id');
      $q->addField('mvv', 'value', 'mvv_value');
      $q->addField('mvf', 'id', 'mvf_id');
      $q->addField('mvf', 'value', 'mvf_value');
      $q->addField('mts', 'id', 'mts_id');
      $q->addField('mts', 'value', 'mts_value');
      $q->orderBy('mvf.value ASC, mvv.value', 'ASC');
      $result = $q->execute();

      while($res_obj = $result->fetchObject()) {

        if(!isset($this->filterMapping[$res_obj->mts_id])) {
          $this->filterMapping[$res_obj->mts_id] = array(
            self::PARAM_TERM => $res_obj->mts_value,
            self::PARAM_FILTERS => array(),
          );
        }

        if(!isset($this->filterMapping[$res_obj->mts_id][self::PARAM_FILTERS][$res_obj->mvf_id])) {
          $this->filterMapping[$res_obj->mts_id][self::PARAM_FILTERS][$res_obj->mvf_id] = array(
            self::PARAM_TERM => $res_obj->mvf_value,
            self::PARAM_MAPPING => array(),
          );
        }

        $this->filterMapping[$res_obj->mts_id][self::PARAM_FILTERS][$res_obj->mvf_id][self::PARAM_MAPPING][] = $res_obj->mvv_id;
      }

      // Cache result
      cache_set(self::CACHE_MAPPING_KEY, $this->filterMapping, 'cache');
    }
  }
}
