<?php
// $id:$

/**
 * @file
 * wss_sodis module helper class.
 */

namespace publicplan\lls;

use publicplan\wss\sodis\Service;

class SodisHelper extends WssHelper {

  /**
   * Mapping array for unifying filter values.
   * The display name (key) have to be in the value array.
   *
   * @var array
   */
  protected static $filterMapping = array(
    Service::PARAM__LANGUAGE => array(
      'Deutsch' => array('de', 'Deutsch', 'de(a), en', 'de(a)'),
      'Englisch' => array('en', 'Englisch'),
      'Französisch' => array('fr', 'Französisch'),
      'Spanisch' => array('es', 'Spanisch'),
      'Türkisch' => array('tr', 'tk', 'Türkisch'),
      'Russisch' => array('ru', 'Russisch'),
      'Arabisch' => array('ar', 'Arabisch'),
      'Niederländisch' => array('nl', 'Niederländisch'),
      'Griechisch' => array('el', 'Griechisch'),
      'ohne Angabe' => array('x-none', 'Unbekannt', 'ohne Angabe'),
    ),
    Service::PARAM__COPYRIGHT => array(
      'CC-BY-SA' => array('CC-BY-SA', 'CC-by-sa', 'cc-by-sa'),
      'CC-BY-NC-ND' => array('CC-BY-NC-ND', 'CC-by-nc-nd', 'cc-by-nc-nd'),
      'CC-BY-NC' => array('CC-BY-NC', 'CC-by-nc', 'cc-by-nc'),
      'CC-BY-NC-SA' => array('CC-BY-NC-SA', 'CC-by-nc-sa', 'cc-by-nc-sa'),
      'CC-BY' => array('CC-BY', 'CC-by', 'cc-by'),
      'CC-BY-ND' => array('CC-BY-ND', 'CC-by-nd', 'cc-by-nd'),
      'Sonstige Lizenz' => array('sonstige Lizenz', 'Sonstige Lizenz', 'sonstige lizenz'),
      'Gemeinfrei' => array('gemeinfrei', 'Gemeinfrei'),
      'Keine Angabe' => array('keine angabe', 'keine Angabe', 'Keine Angabe'),
      'Unbekannt' => array('x-none', 'Unbekannt'),
    ),
  );

  /**
   * Mapping array for unifying filter values in reversed order.
   *
   * @var array
   */
  protected static $reversedFilterMapping;

  /**
   * Mapping array for getting the unifying filter value and its siblings.
   *
   * @var array
   */
  protected static $reversedFilterMappingSiblings;

  /**
   * A list of mapped values that are already present in the filter value.
   *
   * @var array
   */
  protected static $reversedFilterMappingHits;

  const CREATIVE_COMMONS_ICONS_DIR = 'img/cc-icons';

  /**
   * Generates the reversed filter mapping array from the mapping array.
   *
   * @return string
   *   The reversed filter mapping array from the mapping array.
   */
  protected function createReversedFilterMappings() {

    if(!isset(self::$reversedFilterMapping)) {

      self::$reversedFilterMapping = array();
      self::$reversedFilterMappingSiblings = array();

      foreach(self::$filterMapping as $filter => $filter_values) {

        self::$reversedFilterMapping[$filter] = array();
        self::$reversedFilterMappingSiblings[$filter] = array();

        foreach($filter_values as $key => $values) {
          foreach($values as $value) {
            self::$reversedFilterMapping[$filter][$value] = $key;
            self::$reversedFilterMappingSiblings[$filter][$value] = $values;
          }
        }
      }
    }
  }

  /**
   * Initialize parameter mapping array.
   */
  protected function initializeParameterMappings() {
    $this->parameterMappings = array(
      LearnlineSearch::PARAM__COMPETENCIES => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY | self::TYPE__HIDDEN_SIDEBAR,
        self::MAP_PARAMETER => Service::PARAM__COMPETENCY,
      ),
      LearnlineSearch::PARAM__CONTENTTYPES => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => Service::PARAM__RESOURCE_TYPE,
      ),
      LearnlineSearch::PARAM__CONTEXTS => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY | self::TYPE__HIDDEN_SIDEBAR,
        self::MAP_PARAMETER => Service::PARAM__CONTEXT,
      ),
      LearnlineSearch::PARAM__KEYWORDS => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => Service::PARAM__KEYWORD,
      ),
      LearnlineSearch::PARAM__LANGUAGES => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => Service::PARAM__LANGUAGE,
      ),
      LearnlineSearch::PARAM__COPYRIGHTS => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => Service::PARAM__COPYRIGHT,
      ),
      LearnlineSearch::PARAM__DISCIPLINES => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY | self::TYPE__HIDDEN_SIDEBAR,
        self::MAP_PARAMETER => Service::PARAM__DISCIPLINE,
      ),
      LearnlineSearch::PARAM__PROVIDERS => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY | self::TYPE__HIDDEN_SIDEBAR,
        self::MAP_PARAMETER => Service::PARAM__PROVIDER,
      ),
      LearnlineSearch::PARAM__PUBLISHERS => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__IS_ARRAY,
        self::MAP_PARAMETER => Service::PARAM__PUBLISHER,
      ),
      LearnlineSearch::PARAM__PAGE => array(
        self::MAP_TYPE => self::TYPE__CALLBACK | self::TYPE__NO_FILTER,
        self::MAP_CALLBACK => array($this, 'mapPage'),
        self::MAP_DEFAULT => 1,
      ),
      LearnlineSearch::PARAM__SORT => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__NO_FILTER,
        self::MAP_PARAMETER => Service::PARAM__SORT,
        self::MAP_DEFAULT => FALSE,
        self::MAP_VALUES => array(
          LearnlineSearch::SORT_BY__RELEVANCE => FALSE,
          LearnlineSearch::SORT_BY__DATE => Service::SORT_IMPORT_DATE,
          Service::SORT_HEADER_DATE => Service::SORT_HEADER_DATE,
          Service::SORT_CREATOR_DATE => Service::SORT_CREATOR_DATE,
          Service::SORT_IMPORT_DATE => Service::SORT_IMPORT_DATE,
          Service::SORT_PROVIDER_DATE => Service::SORT_PROVIDER_DATE,
          Service::SORT_PUBLISHER_DATE => Service::SORT_PUBLISHER_DATE,
        ),
      ),
      LearnlineSearch::PARAM__ORDER => array(
        self::MAP_TYPE => self::TYPE__DEFAULT | self::TYPE__NO_FILTER,
        self::MAP_PARAMETER => Service::PARAM__ORDER,
        self::MAP_DEFAULT => Service::ORDER_ASC,
        self::MAP_VALUES => array(
          LearnlineSearch::ORDER__ASCENDING => Service::ORDER_ASC,
          LearnlineSearch::ORDER__DESCENDING => Service::ORDER_DESC,
        ),
      ),
    );
  }

  public function mapParameters($learnline_parameters, $service) {
    parent::mapParameters($learnline_parameters, $service);
    if (empty($this->outputParameters[Service::PARAM__SIZE])) {
      $this->mapPage();
    }

    return $this->outputParameters;
  }

  /**
   * Mapping callback for page parameter (LearnlineSearch::PARAM__PAGE).
   *
   * For usability we start counting our result pages with 1 for the first page!
   *
   * @param int $page
   *   Page number to map.
   */
  protected function mapPage($page = 1) {
    $size = LearnlineSearch::getSetting(LearnlineSearch::SETTING__RESULTS_PER_PAGE);
    $from = ($size * ($page > 0 ? $page : 1)) - $size;
    $this->outputParameters[Service::PARAM__SIZE] = $size;
    $this->outputParameters[Service::PARAM__FROM] = $from;
  }

  /**
   * Appends the filter mapping array.
   *
   * The resulting array can be used to determine the display facet value of
   * filters.
   *
   * @param string $filter_name
   *   The filter name.
   * @param string $value
   *   The filter value (term may vary by provider).
   *
   * @return string
   *   Display value (display facet) of a filter value.
   */
  public function mapFilter($filter_name, $value) {
    static $mapper;

    // Init mapper data on first call
    if (!isset($mapper[$filter_name])) {

      $mapper[$filter_name] = array();

      foreach (self::$filterMapping[$filter_name] as $display_name => $matches) {
        foreach ($matches as $match) {
          $mapper[$filter_name][$match] = $display_name;
        }
      }
    }

    $lower_value = mb_strtolower($value);

    return isset($mapper[$filter_name][$lower_value]) ? $mapper[$filter_name][$lower_value] : FALSE;
  }

  /**
   * Custom sorting callback.
   *
   * @param array $a
   *   Sorting object 1.
   * @param array $b
   *   Sorting object 2.
   *
   * @return int
   *   Integer indicating relative priority of $a and $b.
   */
  private function facetSort($a, $b) {
    if (isset($a[Service::ACTIVE]) && !isset($b[Service::ACTIVE])) {
      return -1;
    }
    elseif (!isset($a[Service::ACTIVE]) && isset($b[Service::ACTIVE])) {
      return 1;
    }
    elseif ($a[Service::COUNT] !== $b[Service::COUNT]) {
      return $b[Service::COUNT] - $a[Service::COUNT];
    }
    else {
      return strnatcmp($b[Service::DISPLAY_VALUE], $a[Service::DISPLAY_VALUE]);
    }
  }

  /**
   * Adds a language to the given or current query parameters.
   *
   * @param string $lang
   *   Language to add.
   * @param array  $query_parameters
   *   Optional query parameters. If omitted, the current request parameters
   *   will be used.
   *
   * @return array
   *   Resulting query parameters.
   */
  /*
  public function queryAddLanguage($lang, $query_parameters = NULL) {
    if (!isset($query_parameters)) {
      $query_parameters = \drupal_get_query_parameters(NULL, array('q', Service::PARAM__PAGE));
    }
    $query_parameters[Service::PARAM__LANGUAGES] = array_merge(
        isset($query_parameters[Service::PARAM__LANGUAGES]) ?
          $query_parameters[Service::PARAM__LANGUAGES] : array(),
        isset(self::$languageMapping[$lang]) ?
          \array_merge(self::$languageMapping[$lang], array($lang)) : array($lang));

    return $query_parameters;
  }
  */

  /**
   * Removes a language from given or current query parameter array.
   *
   * @param string $lang
   *   Language to be remove.
   * @param array $query_parameters
   *   Optional query parameters. If omitted the current request parameters are
   *   taken.
   *
   * @return array
   *   Array of remaining query parameters.
   */
  /*
  public function queryRemoveLanguage($lang, $query_parameters = NULL) {
    if (!isset($query_parameters)) {
      $query_parameters = \drupal_get_query_parameters(NULL, array('q',
          Service::PARAM__PAGE));
    }
    // There is no language parameter to remove. So we can just return.
    if (!isset($query_parameters[Service::PARAM__LANGUAGES])) {
      return $query_parameters;
    }

    if (isset(self::$languageMapping[$lang])) {
      if (is_int($idx = \array_search($lang, $query_parameters[Service::PARAM__LANGUAGES]))) {
        unset($query_parameters[Service::PARAM__LANGUAGES][$idx]);
      }
      foreach (self::$languageMapping[$lang] as $lang_short) {
        if (is_int($idx = \array_search($lang_short, $query_parameters[Service::PARAM__LANGUAGES]))) {
          unset($query_parameters[Service::PARAM__LANGUAGES][$idx]);
        }
      }
    }
    elseif (\is_int($idx = \array_search($lang, $query_parameters[Service::PARAM__LANGUAGES]))) {
      unset($query_parameters[Service::PARAM__LANGUAGES][$idx]);
    }

    return $query_parameters;
  }
  */

  /**
   * Get an array of active facets based on the given query parameter array.
   *
   * The array being returned is a nested structure with each parameter value,
   * as element, containing the query parameter, the parameter value itself,
   * the human readable parameter name and a an array structure for building
   * a query without the specific parameter value.
   *
   * @param array $query_parameters
   *   Query parameter array.
   *
   * @return array
   *   Nested array structure as described above.
   */
  public function getActiveFilters($query_parameters) {

    $active_filters = array();

    foreach ($query_parameters as $query_param => $query_value) {

      if (empty($query_value) || !isset($this->parameterMappings[$query_param]) ||
          $this->isTypeNoFilter($this->parameterMappings[$query_param][self::MAP_TYPE])) {
        continue;
      }

      $map_param = $this->parameterMappings[$query_param][self::MAP_PARAMETER];
      $reversed_mapping = NULL;
      $reversed_sibling_mapping = NULL;
      if (isset(self::$reversedFilterMapping[$map_param])) {
        $reversed_mapping = self::$reversedFilterMapping[$map_param];
        $reversed_sibling_mapping = self::$reversedFilterMappingSiblings[$map_param];
      }

      if(!isset(self::$reversedFilterMappingHits[$query_param])) {
        self::$reversedFilterMappingHits[$query_param] = array();
      }

      if (is_array($query_value)) {

        foreach ($query_value as $value) {

          if (empty($value)) {
            continue;
          }

          if($reversed_mapping !== NULL && isset($reversed_mapping[$value])) {
            $value = $reversed_mapping[$value];
          }

          if(!in_array($value, self::$reversedFilterMappingHits[$query_param], false)) {
            $active_filters[] = $this->getActiveFilter($query_parameters, $query_param, $value, $reversed_sibling_mapping);
            self::$reversedFilterMappingHits[$query_param][] = $value;
          }
        }
      }
      else {
        if(!in_array($query_value, self::$reversedFilterMappingHits[$query_param], false)) {
          $active_filters[] = $this->getActiveFilter($query_parameters, $query_param, $query_value, $reversed_sibling_mapping);
          self::$reversedFilterMappingHits[$query_param][] = $query_value;
        }
      }
    }

    return $active_filters;
  }

  /**
   * Get/Build one element for the array structure returned by getActiveFacets().
   *
   * @param array $query
   *   Query parameter array.
   * @param string $query_param
   *   A query parameter name.
   * @param string $query_value
   *   A query parameter value.
   * @param array $reversed_sibling_mapping
   *   An optional list of sibling mapping values.
   *
   * @return array
   *   Array containing all necessary information for the given query parameter
   *   value.
   *
   * @throws \Exception
   *   Throws an exception on misconfiguration.
   */
  private function getActiveFilter($query, $query_param, $query_value, $reversed_sibling_mapping = NULL) {

    // Remove all mapping values from query
    if(isset($reversed_sibling_mapping[$query_value])) {
      foreach($reversed_sibling_mapping[$query_value] as $map_value) {
        $query = $this->queryRemove($query, $query_param, $map_value);
      }
    }
    else {
      $query = $this->queryRemove($query, $query_param, $query_value);
    }

    return array(
      'param' => $query_param,
      'value' => $query_value,
      'title' => LearnlineSearch::getInstance()->getFilterTitle($query_param),
      'query' => $this->queryToString($query),
    );
  }

  /**
   * Return a nested array of available filter parameters and desired values.
   *
   * @param array $query_parameters
   *   Current query parameter array.
   * @param array $filter_array
   *   Filter/facet array as returned from
   *   `\publicplan\wss\sodis\Response->getData('facets')`.
   *
   * @return array
   *   Array Structure containing human readable titles of the parameters,
   *   the desired values, match count and the altered query string for each
   *   value.
   *
   * @throws \Exception
   *   Throws an exception on misconfiguration of \publicplan\lls\LearnlineSearch.
   */
  public function getAvailableFilters($query_parameters, $filter_array) {

	  $filters = array();

    // Began with iteration of reverseParameterMappings to honor the defined order.
    foreach ($this->reverseParameterMappings as $filter_name => $mapping) {

      $param = $mapping[self::MAP_PARAMETER];

      // Skip non-existing filters or those which are defined as hidden in sidebar.
      if ($this->isTypeHiddenSidebar($this->parameterMappings[$param][self::MAP_TYPE])) {
		    continue;
      }

      if (!isset($filter_array[$filter_name]) || empty($filter_array[$filter_name])) {
        continue;
      }

      $filter_values = $filter_array[$filter_name];
      $available_filters = array();

      // Iterate filter values.
      foreach ($filter_values as $value => $count) {

        // Skip values, which are already set.
        if (isset($query_parameters[$param]) && (
            ($this->isTypeArray($mapping[self::MAP_TYPE]) && is_int(array_search($value, $query_parameters[$param]))) ||
            (!$this->isTypeArray($mapping[self::MAP_TYPE]) && $query_parameters[$param] === $value))) {
          continue;
        }

        $query_params = $this->queryAdd($query_parameters, $param, $value);

        // Mapping for filter is available
        if(isset(self::$filterMapping[$filter_name])) {

          $count = 0;
          $value = $this->mapFilter($filter_name, $value);

          $has_value = false;
          foreach($available_filters as $filter_data) {
            if($filter_data['value'] === $value) {
              $has_value = true;
              break;
            }
          }

          if(empty($value) || $has_value) {
            continue;
          }

          $query_params = array();

          // Loop facets $query_parameters
          foreach (self::$filterMapping[$filter_name][$value] as $k => $v) {

            // Manipulate counts with hidden filter mappings
            if(isset($filter_values[$v])) {
              $count += $filter_values[$v];
            }

            $query_params[] = $v;
          }

          $query_params = $this->queryAdd($query_parameters, $param, $query_params);
        }

        $available_filters[] = array(
          'value' => $value,
          'count' => $count,
          'query' => $this->queryToString($query_params),
        );
      }
      // Only add the filter, if there are actually values left, which are not
      // already set.
      if (!empty($available_filters)) {
        $filters[$param] = array(
          'title' => LearnlineSearch::getInstance()->getFilterTitle($param),
          'children' => $available_filters,
        );
      }
    }

    return $filters;
  }

  public function linkFacet($facet, $value) {
    global $base_url;
    static $current_path, $current_query;

    if (!isset($current_path)) {
      $current_path = request_path();
      $current_query = drupal_get_query_parameters();
    }

    $query = $current_query;

    if (preg_match('/_orig$/i', $facet)) {
      $facet = substr($facet, 0, strlen($facet) -5);
    }

    if (isset($this->reverseParameterMappings[$facet])) {
      $param = $this->reverseParameterMappings[$facet][self::MAP_PARAMETER];
      if ($this->isTypeArray($this->reverseParameterMappings[$facet][self::MAP_TYPE])) {
        if (!isset($query[$param])) {
          $query[$param] = array();
        }
        if (!is_array($query[$param])) {
          $query[$param] = array($query[$param]);
        }

        // Check if value already in array
        if (!in_array($value, $query[$param], false)) {
          $query[$param][] = $value;
        }
      }
      else {
        $query[$param] = $value;
      }

      // Add mappings to query array
      if(isset(self::$reversedFilterMappingSiblings[$facet]) && isset(self::$reversedFilterMappingSiblings[$facet][$value])) {
        foreach (self::$reversedFilterMappingSiblings[$facet][$value] as $match) {
          if(!in_array($match, $query[$param], false)) {
            $query[$param][] = $match;
          }
        }
      }
    }

    return "{$base_url}/{$current_path}?" . $this->queryToString($query);
  }

  public function queryAdd($query, $param, $value) {
    $query['primary_service'] = 'sodis';
    return parent::queryAdd($query, $param, $value);
  }

  public function queryRemove($query, $param, $value = NULL) {
    $query['primary_service'] = 'sodis';
    return parent::queryRemove($query, $param, $value);
  }

  /**
   * Gets the value from the reversed mapping array or returns the value as it is.
   *
   * @param $facet
   *  The name of the filter type.
   * @param $value
   *  The value for mapping check.
   *
   * @return string
   *  The value from the reversed mapping array or returns the value as it is.
   */
  public static function prepareMappingValues($facet, $value) {

    $value = trim($value);

    // Get mapped value
    if(isset(self::$reversedFilterMapping[$facet][$value])) {
      return self::$reversedFilterMapping[$facet][$value];
    }

    // Return the value as it is
    return $value;
  }

  /**
   * Build a simple array containing the keys 'parent' and children 'children'.
   *
   * @param array $result
   *  The search result array.
   * @return array
   *  An array containing parent and children elements regardless of the role
   *  of the current element itself.
   */
  static public function relationTree($result) {
    $tree = array();

    if (!empty($result['parent']) && !empty($result['siblings'])) {
      $tree['parent'] = $result['parent'];
      $tree['children'] = $result['siblings'];
    }
    if (!empty($result['children'])) {
      $tree['children'] = $result['children'];
      $tree['parent'] = array(
        'id' => $result['id'],
        'title' => $result['title'],
      );
    }
    if (!empty($tree['children'])) {
      usort($tree['children'], 'self::relationTitleSort');
    }

    return $tree;
  }

  static private function relationTitleSort($a, $b) {
    return strnatcasecmp($a['title'], $b['title']);
  }

  static public function searchLink($id) {
    global $base_url;

    $url = "{$base_url}/suche/" . urlencode($id);

    return check_url($url);
  }
}
