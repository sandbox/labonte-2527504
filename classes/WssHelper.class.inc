<?php
/**
 * @file
 * WssHelper class implementing common methods for all WSS helper classes.
 */

namespace publicplan\lls;

/**
 * Abstract helper class for extension.
 */
abstract class WssHelper {

  /**
   * Mapping key: Parameter display title.
   */
  const MAP_TITLE = 'title';

  /**
   * Mapping key: Type.
   */
  const MAP_TYPE = 'type';

  /**
   * Mapping type: Default (nothing special).
   */
  const TYPE__DEFAULT = 1;

  /**
   * Mapping type: Is array.
   */
  const TYPE__IS_ARRAY = 2;

  /**
   * Mapping type: Callback.
   */
  const TYPE__CALLBACK = 4;

  /**
   * Mapping type: Do not handle as filter parameter.
   */
  const TYPE__NO_FILTER = 8;

  /**
   * Mapping type: Hide filter in sidebar.
   */
  const TYPE__HIDDEN_SIDEBAR = 16;

  /**
   * Define mapping callback.
   * @see self::TYPE__CALLBACK
   */
  const MAP_CALLBACK = 'callback_func';

  /**
   * Mapping key: Target parameter.
   */
  const MAP_PARAMETER = 'parameter';

  /**
   * Mapping key: Optional parameter value mapping.
   *
   * Expects an associative array as value. The array should have the input
   * values as keys and the corresponding output values as values.
   */
  const MAP_VALUES = 'values';

  /**
   * Mapping key: Optionally define a default value.
   */
  const MAP_DEFAULT = 'default';

  /**
   * Mapping key: Optional definiton of the reverse mapping behavoir.
   *
   * Expects an associative array as value. Takes the same keys as the regular
   * mapping array. Omitted keys will be read from the regular mapping
   * definition.
   */
  const MAP_REVERSE = 'reverse_mapping';

  /**
   * Webservice specific parameter mappings.
   *
   * @var array
   */
  protected $parameterMappings = array();

  /**
   * Autofilled lookup array for reverse parameter mapping.
   *
   * @var array
   */
  protected $reverseParameterMappings = array();

  /**
   * Most recently mapped output parameters.
   *
   * @var array
   */
  protected $outputParameters = array();

  /**
   * Reverse mapped output parameters.
   *
   * @var array
   */
  protected $outputReverseParameters = array();

  /**
   * Class object reference storage.
   *
   * @var array
   */
  protected static $object = array();

  /**
   * Protected constructor.
   *
   * Use get() method instead.
   */
  protected function __construct() {
    $this->initializeParameterMappings();
    $this->createReversedFilterMappings();
    $this->buildReverseMap();
  }

  /**
   * Return object instance.
   *
   * @return \publicplan\lls\WssHelper
   *   The appropriate class object instance.
   */
  public static function get() {
    $class_name = get_called_class();
    if (!isset(self::$object[$class_name])) {
      self::$object[$class_name] = new $class_name();
    }

    return self::$object[$class_name];
  }

  /**
   * Map common search parameters.
   *
   * @param array $learnline_parameters
   *   Learnline (lls module) specific search parameters.
   *
   * @return array
   *   Parameters mapped for use with a specific wss Request.
   */
  public function mapParameters($learnline_parameters, $service) {

    $this->outputParameters = array();

    foreach ($learnline_parameters as $input_parameter => $value) {

      if (!isset($this->parameterMappings[$input_parameter])) {
        continue;
      }

      $mapping = &$this->parameterMappings[$input_parameter];
      $mapping_value = isset($mapping[self::MAP_VALUES]) ?
        $this->mapValue($value, $mapping) : $value;

      if ($mapping[self::MAP_TYPE] & self::TYPE__DEFAULT) {
        $this->mapSimple($mapping_value, $mapping);
      }
      elseif ($mapping[self::MAP_TYPE] & self::TYPE__CALLBACK) {
        $this->mapCallback($mapping_value, $mapping, $this->parameterMappings);
      }
    }

    return $this->outputParameters;
  }

  /**
   * Get already mapped parameter values.
   *
   * @param string $learnline_parameter
   *   Learnline parameter name.
   *
   * @return mixed
   *   Mapped parameters or NULL.
   */
  public function getMappedParameter($learnline_parameter = NULL) {
    if (!$learnline_parameter) {
      return $this->outputParameters;
    }
    if (!array_key_exists($learnline_parameter, $this->outputParameters)) {
      return NULL;
    }

    return $this->outputParameters[$learnline_parameter];
  }

  /**
   * Reverse parameter mapping.
   *
   * @param array $service_parameters
   *   Service specific parameter name.
   *
   * @return array
   *   (Common) Learnline parameters.
   */
  public function reverseMapParameters($service_parameters) {
    if (!isset($this->reverseParameterMappings)) {
      $this->buildReverseMap();
    }

    $learnline_parameters = array();
    foreach ($service_parameters as $input_parameter => $value) {
      if (!isset($this->reverseParameterMappings[$input_parameter])) {
        continue;
      }
      $mapping = &$this->reverseParameterMappings[$input_parameter];
      $mapping_value = isset($mapping[self::MAP_VALUES]) ?
        $this->mapValue($value, $mapping) : $value;
      if ($mapping[self::MAP_TYPE] & self::TYPE__DEFAULT) {
        $this->mapSimple($mapping_value, $mapping, $this->outputReverseParameters);
      }
      elseif ($mapping[self::MAP_TYPE] & self::TYPE__CALLBACK) {
        $this->mapCallback($mapping_value, $mapping, $this->outputReverseParameters);
      }
    }
  }

  /**
   * Get already reverse mapped parameter values.
   *
   * @param string $service_parameter
   *   Learnline parameter name.
   *
   * @return mixed
   *   Mapped parameters or NULL.
   */
  public function getReverseMappedParameter($service_parameter = NULL) {
    if (!$service_parameter) {
      return $this->outputReverseParameters;
    }
    if (!array_key_exists($service_parameter, $this->outputReverseParameters)) {
      return NULL;
    }

    return $this->outputReverseParameters[$service_parameter];
  }

  /**
   * Build the reverse mapping array.
   */
  private function buildReverseMap() {

    foreach ($this->parameterMappings as $learnline_parameter => $service_mapping) {
      if(isset($service_mapping[self::MAP_PARAMETER]) === true) {
        $service_parameter = $service_mapping[self::MAP_PARAMETER];
        $this->reverseParameterMappings[$service_parameter] = $service_mapping;
        $this->reverseParameterMappings[$service_parameter][self::MAP_PARAMETER] = $learnline_parameter;
        if (isset($service_mapping[self::MAP_REVERSE])) {
          $this->reverseParameterMappings[$service_parameter] = array_replace_recursive(
            $this->reverseParameterMappings[$service_parameter],
            $service_mapping[self::MAP_REVERSE]
          );
          unset($this->reverseParameterMappings[$service_parameter][self::MAP_REVERSE]);
        }
      }
    }
  }

  /**
   * Map input values to fit the underlying webservice parameters.
   *
   * @param mixed $value
   *   Input value.
   * @param array $mapping
   *   Mapping of the specific parameter.
   *
   * @return mixed
   *   Mapped value.
   */
  private function mapValue($value, &$mapping) {
    if (!isset($mapping[self::MAP_VALUES][$value])) {
      return $value;
    }

    return $mapping[self::MAP_VALUES][$value];
  }

  /**
   * Perform simple mapping for a single parameter.
   *
   * @param mixed $value
   *   Parameter value.
   * @param array $mapping
   *   The appropriate mapping sub-array for the given parameter value.
   */
  private function mapSimple($value, &$mapping, &$target_array = NULL) {

    if (!$target_array) {
      $target_array = &$this->outputParameters;
    }
    if ($mapping[self::MAP_TYPE] & self::TYPE__IS_ARRAY) {
      if (!isset($target_array[$mapping[self::MAP_PARAMETER]]) || !is_array($target_array[$mapping[self::MAP_PARAMETER]])) {
        $target_array[$mapping[self::MAP_PARAMETER]] = array();
      }
      if (is_array($value)) {
        $target_array[$mapping[self::MAP_PARAMETER]] += $value;
      }
      else {
        $target_array[$mapping[self::MAP_PARAMETER]][] = $value;
      }
    }
    else {
      $target_array[$mapping[self::MAP_PARAMETER]] = $value;
    }
  }

  /**
   * Perform callback mapping for a single parameter.
   *
   * @param mixed $value
   *   Parameter value.
   * @param array $mapping
   *   The appropriate mapping sub-array for the given parameter value.
   */
  private function mapCallback($value, &$mapping, &$target_array) {
    if (!$target_array) {
      $target_array = &$this->outputParameters;
    }
    if (is_callable($mapping[self::MAP_CALLBACK])) {
      call_user_func_array($mapping[self::MAP_CALLBACK], array($value, $target_array));
    }
  }

  /**
   * Returns the human readable title for a filter/parameter value.
   *
   * If the title is not set, the input filter name is returned.
   *
   * @param string $filter_name
   * @return string
   */
  public function getFilterTitle($filter_name) {
    if (!isset($this->parameterMappings[$filter_name])
        || !isset($this->parameterMappings[$filter_name][self::MAP_TITLE])) {
        return $filter_name;
    }

    return $this->parameterMappings[$filter_name][self::MAP_TITLE];
  }

  /**
   * Check mapping Type: Is array?
   *
   * @param int $type
   *   A type definition value.
   *
   * @return bool
   *   Whether the type value indicates that parameter value has to be an array.
   */
  public function isTypeArray($type) {
    return (bool) ($type & self::TYPE__IS_ARRAY);
  }

  /**
   * Check mapping Type: Is default?
   *
   * @param int $type
   *   A type definition value.
   *
   * @return bool
   *   Whether the type value indicates a default (array based) mapping.
   */
  public function isTypeDefault($type) {
    return (bool) ($type & self::TYPE__DEFAULT);
  }

  /**
   * Check mapping Type: Is callback?
   *
   * @param int $type
   *   A type definition value.
   *
   * @return bool
   *   Whether the type value indicates a callback mapping.
   */
  public function isTypeCallback($type) {
    return (bool) ($type & self::TYPE__CALLBACK);
  }

  /**
   * Check mapping Type: Is no filter?
   *
   * @param int $type
   *   A type definition value.
   *
   * @return bool
   *   Whether the type value indicates the parameter cannot be used as a filter.
   */
  public function isTypeNoFilter($type) {
    return (bool) ($type & self::TYPE__NO_FILTER);
  }

  /**
   * Check mapping Type: Is filter hidden in sidebar?
   *
   * @param int $type
   *   A type definition value.
   *
   * @return bool
   *   Whether the type value indicates the parameter should not show up in
   *   sidebars.
   */
  public function isTypeHiddenSidebar($type) {
    return (bool) ($type & self::TYPE__HIDDEN_SIDEBAR);
  }

  /**
   * Converts a query array into a query string.
   *
   * @param array   $queryArray Query array as retrieved from drupal_get_query_array().
   * @return string             The appropriate query string starting with '?'.
   */
  public function queryToString($queryArray) {
    $query = array();
    foreach ($queryArray as $key => $value) {
      if ($key === 'q') {
        // We will remove the q parameter, because it is reserved by Drupal to
        // keep the current request path.
        continue;
      }

      if (is_array($value)) {
        foreach ($value as $arr_value) {
          $query[] = $key . '[]=' . urlencode($arr_value);
        }
      }
      else {
        $query[] = $key . '=' . urlencode($value);
      }
    }

    return implode('&', $query);
  }

  /**
   * Add query parameter based on the parameter mapping array.
   *
   * @param array $query Current query parameters.
   * @param string $param Parameter to add.
   * @param string $value Value to add.
   * @return array The new query parameter array.
   */
  public function queryAdd($query, $param, $value) {
    $query['page'] = 1;
    $value_is_array = is_array($value);
    if ($this->isTypeArray($this->parameterMappings[$param][self::MAP_TYPE])) {
      if (!isset($query[$param])) {
        $query[$param] = array();
      }
      if($value_is_array) {
        $query[$param] = $this->queryAddArray($query[$param], $value);
      }
      else {
        $query[$param][] = $value;
      }
    }
    else {
      if($value_is_array) {
        if (!isset($query[$param])) {
          $query[$param] = array();
        }
        $query[$param] = $this->queryAddArray($query[$param], $value);
      }
      else {
        $query[$param] = $value;
      }
    }

    return $query;
  }

  /**
   * Adds the given array values to the query array.
   *
   * @param array $query
   *   The query parameter array.
   * @param array $value
   *   The parameter  array with values to add.
   *
   * @return array
   *   The extended query parameter array.
   */
  private function queryAddArray(&$query, $value) {
    foreach($value as $val) {
      $query[] = $val;
    }

    return $query;
  }

  /**
   * Remove a query parameter or parameter value.
   *
   * @param array $query
   *   The query parameter array.
   * @param string $param
   *   The parameter for which to remove a value.
   * @param string $value
   *   The parameter value to remove.
   *
   * @return array
   *   The reduced query parameter array.
   */
  public function queryRemove($query, $param, $value = NULL) {
    $query[LearnlineSearch::PARAM__PAGE] = 1;
    if (!isset($query[$param])) {
      return $query;
    }
    elseif (is_array($query[$param])) {
      if (!isset($value)) {
        unset($query[$param]);
      }
      else {
        $key = array_search($value, $query[$param]);
        unset($query[$param][$key]);
      }
    }
    else {
      unset($query[$param]);
    }

    return $query;
  }

  /**
   * Actually define the parameter mappings.
   *
   * Would be nicer to simply do that in the array declaration, but in a
   * function we are able to use the object context and bitwise OR operator.
   */
  abstract protected function initializeParameterMappings();

  public static function getMoreFilters($result, $display) {

    $html = '';

    foreach($display as $key => $groups) {

      $html .= '<div class="col-md-4 col-xs-12">';

      foreach($groups as $group_id => $group_data) {

        $html .= '<h4>' . $group_data['display_name'] . '</h4>';
        $link_id = (!empty($group_data['filter_name'])) ? $group_data['filter_name'] : $group_id;

        if(!empty($result[$group_id])) {
          if(is_array($result[$group_id])) {
            foreach($result[$group_id] as $group_value) {
              $html .= self::getMoreValue($group_data['is_attribute'], $link_id, $group_value);
            }
          }
          else {
            $html .= self::getMoreValue($group_data['is_attribute'], $link_id, $result[$group_id]);
          }
        }
        else if($group_data['is_attribute']) {
          $html .= self::getMoreValue($group_data['is_attribute'], $link_id, t('Keine Angabe'));
        }
      }

      $html .= '</div>';
    }

    return $html;
  }

  private static function getMoreValue($is_attribute, $id, $value) {
    $html = '';

    if(!$is_attribute) {
      $html .= '<a href="' . LfsHelper::get()->linkFilter($id, $value) . '" class="' . $id . '">' . $value . '</a>';
    }
    else {
      $html .= '<span class="' . $id . '">' . $value . '</span>';
    }

    return $html;
  }

  /**
   * Callback for array sorting by title.
   *
   * @param string  $a  A string to compare to another string.
   * @param string  $b  A string to compare to another string.
   *
   * @return  int   Returns < 0 if str1 is less than str2; > 0 if str1 is greater than str2, and 0 if they are equal.
   */
  protected static function compareByName($a, $b) {
    return strcasecmp($a['title'], $b['title']);
  }
}
