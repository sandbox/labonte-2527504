<?php
/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 *
 * Variables:
 * - $base_url: Drupal's global base_url.
 * - $logo: theme_get_setting('logo_path') ?: theme_get_setting('logo').
 * - $site_name: variable_get('site_name', '').
 * - $slogan: variable_get('site_slogan', '').
 */
?>
<form id="search-box" accept-charset="UTF-8" action="/suche" method="get">
  <div class="row">
    <div class="col left" id="search-logo-container">
      <a href="/" title="<?php print $slogan; ?>">
        <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"/>
      </a>
    </div>
    <div class="subrow col left">
      <div class="col left" id="search-input-container">
        <div>
          <div id="search-input-wrapper">
            <input id="search-input" class="form-text required" tabindex="1" size="60" type="text" name="term" placeholder="Suche..."/>
          </div>
        </div>
      </div>
      <div class="col right" id="search-button-container">
        <div id="search-button-wrapper">
          <?php /* #0272C1 */ ?>
          <button id="search-button" type="submit" title="<?php t('Start searching'); ?>">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</form>
