<?php
/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 *
 * Variables:
 * - $base_url: Drupal base url.
 * - $module_path: Filesystem path to the module.
 * - $active_tab: Initial service identifier/name.
 * - $results: Service response results data array.
 */

?>
<?php foreach ($results as $result): ?>
  <?php print theme('lls_search_result_'.$active_tab, compact('result')); ?>
<?php endforeach; ?>
<?php var_dump($results); ?>
