<?php
// $id:$

/**
 * @file
 * Template file for displaying the latest learn:line NRW resources.
 */
?>
<?php if (isset($element['#children']) && is_array($element) && !empty($element['#children'])): ?>
  <?php foreach ($element['#children'] as $teaser): ?>
  <a href="<?php print $teaser['#href']; ?>">
    <div class="lls-most-recent clearfix">
      <h3 class="lls-most-recent-header">
        <?php print $teaser['#header']; ?>
      </h3>
      <?php if ($teaser['#img']): ?>
        <img src="<?php print $teaser['#img']; ?>" />
      <?php endif; ?>
      <p class="lls-most-recent-content">
        <?php print text_summary($teaser['#summary'], NULL, 160); ?>
      </p>
    </div>
  </a>
  <?php endforeach; ?>
<?php else: ?>
<div class="lls-most-recent">
  <p class="lls-most-recent-content">
    <?php print t('The most recent resources are currently unavailable.'); ?>
  </p>
</div>
<?php endif; ?>
