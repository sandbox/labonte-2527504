<?php
// $id:$

/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 */

global $base_url;

/**
 * As long as we cannot stage real API requests, we have to generate fake IDs,
 * to not corrupt the Javascript!!!
 */
//$element['#id'] = $element['#id'] . '-' . rand(0, 999999);
?>
<i class="fa fa-fake-edutags"></i>
<a class="edutags-button edutags-rating" href="#edutags-vote"
   title="<?php print t('Vote on Edutags website'); ?>"
   alt="<?php print t('@voting of 100', array('@voting' => $element['#rating'])); ?>">
<!--  <i class="fa fa-external-link-square"></i>-->
  <div class="edutags-voting">
    <?php if (isset($element['#rating'])): ?>
      <div class="rating-value <?php print $element['#rating_class']; ?>"
           style="width: <?php print $element['#rating']; ?>%;">
      </div>
    <?php endif; ?>
  </div>
</a>
<a class="edutags-toggle" href="#show-edutags">
  <?php 
    $tag_count = count($element['#tags']);
    $tags = $tag_count === 1 ? t('<em>1</em> tag') 
        : t('%tags tags', array('%tags' => $tag_count, '@tags' => $tag_count));
    
    $comment_count = is_array($element['#comments']) ? count($element['#comments']) : 0;
    $comments = $comment_count === 1 ? t('<em>1</em> comment') 
        : t('%comments comments', array('%comments' => $comment_count, '@comments' => $comment_count));
  ?>
  (<?php print $tags . ', ' . $comments; ?>)
</a>
<!--<div class="edutags-toggle" id="toggle-<?php print $element['#id']; ?>">
    <?php print t('Edutags'); ?>
    <i class="fa fa-fake-edutags"></i>
</div>-->
<div class="edutags-tags">
  <h5>Tags:</h5>
<!--  <a class="edutags-button edutags-button-tag fa fa-external-link-square" href="#edutags-tag"
     title="<?php print t('Tag this resource on Edutags website'); ?>"></a>-->
  <ul>
    <li class="edutags-button edutags-button-tag">
      <?php print empty($element['#tags']) ? t('Tag this resource') : t('Add tags'); ?>
    </li>
    <?php foreach ($element['#tags'] as $tag): ?>
      <li title="<?php print t('tagged @count times', array('@count' => $tag['#count'])); ?>">
        <i class="fa fa-tag"></i>&nbsp;<?php print htmlspecialchars($tag['#tag']); ?>
      </li>
    <?php endforeach; ?>
  </ul>
  <div class="clearfix"></div>
</div>
<?php  
  /**
   * We want to avoid recursion in template files for two reasons:
   *  1. We don't want functions inside template files.
   *  2. It's faster without recursions!
   */
?>
<div class="edutags-comments">
  <h5>Kommentare:</h5>
<!--  <a class="edutags-button edutags-button-comment fa fa-external-link-square" href="#edutags-comment"
     title="<?php print t('Comment on Edutags website'); ?>"></a>-->
  <ul>
    <li class="edutags-button edutags-button-comment">
      <?php print t('Add new comment'); ?>
    </li>
    <?php if (isset($element['#comments']) && !empty($element['#comments'])): ?>
      <?php foreach ($element['#comments'] as $comment): ?>
        <li class="edutags-comment">
          <h6>
            <?php print $comment['#title']; ?> 
            <small>
              (<?php print t('written by') . ' <em>' . $comment['#username'] . '</em>'; ?>)
            </small>
          </h6>
          <p><?php print $comment['#content']; ?></p>
          <?php while (false): ?>
            <?php /** @todo Render potential responses. */ ?>
          <?php endwhile; ?>
        </li>
      <?php endforeach; ?>
    <?php else: ?>
<!--      <li class="edutags-comment">
        <?php print t('There are no comments on Edutags for this resource.'); ?>
      </li>-->
    <?php endif; ?>
  </ul>
  <div class="clearfix"></div>
</div>
