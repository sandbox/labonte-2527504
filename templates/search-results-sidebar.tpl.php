<?php
// $id:$

/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 */

/**
 * $element = array(
 *  '#theme' => 'learnline_search_result',
 *  '#results' => array(
 *    0 => array(
 *      '#thumbnail' => array(
 *        '#theme' => 'learnline_search_result_thumbnail',
 *        '#images' => array(
 *          0 => [img src path],
 *          ...
 *        ),
 *      ),
 *    ),
 *    ...
 *  ),
 * );
 */

$lls = LearnlineSearch::getInstance();

?>
<div>
  <h5><?php print $element['#legal_notice']['heading']; ?></h5>
  <div id="rechtshinweis_text">
    <h1><?php print $element['#legal_notice']['heading']; ?></h1>
    <?php print $legal_notice = render($element['#legal_notice']['value']); ?>
  </div>
  <p>
    <?php print truncate_utf8(strip_tags($legal_notice), 85) . '...'; ?>
    <a id="rechtshinweis_show" href="#rechtshinweis">Weiterlesen&nbsp;&raquo;</a>
  </p>
  <?php foreach ($element['#facets'] as $facet_name => $facet): ?>
    <?php if (!empty($facet)): ?>
      <?php $i = 0; // Loop counter ?>
      <h5 style="margin-top: 25px;">
        <?php print $lls->getFilterTitle($facet_name); ?>
      </h5>
      <ul class="search-result-facets">
        <?php foreach ($facet as $values): ?>
          <?php if ($i === 5): ?>
            <div class="content-more">
          <?php endif; ?>
          <?php if (++$i > 10) break; ?>
          <li>
            <?php if (isset($values[LearnlineSearch::ACTIVE])): ?>
              <a href="<?php print $values[LearnlineSearch::ACTIVE]; ?>"
                 style="font-weight: bold;">
                <?php print $lls->facetFilter($facet_name, $values[LearnlineSearch::DISPLAY_VALUE]); ?>
                (<?php print $values[LearnlineSearch::COUNT]; ?>)
                <i class="fa fa-trash-o"></i>
              </a>
            <?php elseif (isset($values[LearnlineSearch::HREF])): ?>
              <a href="<?php print $values[LearnlineSearch::HREF]; ?>">
                <?php print $lls->facetFilter($facet_name, $values[LearnlineSearch::DISPLAY_VALUE]); ?>
                (<?php print $values[LearnlineSearch::COUNT]; ?>)
              </a>
            <?php else: ?>
              <?php $url = $element['#facet_base_url'] . '&' . $facet_name .
                  ($lls->isMultiFacet($facet_name) ? '[]=' : '=') . 
                  urlencode($values[LearnlineSearch::DISPLAY_VALUE]); ?>
              <a href="<?php print $url ?>">
                <?php print $lls->facetFilter($facet_name, $values[LearnlineSearch::DISPLAY_VALUE]); ?>
                (<?php print $values[LearnlineSearch::COUNT]; ?>)
              </a>
            <?php endif; ?>
          </li>
        <?php endforeach; ?>
        <?php if ($i > 5): ?>
          </div>
          <li class="toggle-more">
            <?php print t('more'); ?>
            <i class="fa fa-arrow-down"></i>
          </li>
        <?php endif; ?>
      </ul>
    <?php endif; ?>
  <?php endforeach; ?>
</div>
