<?php
// $id:$

/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 */

if (count($element['#results']) > 1): ?>
<div id="edmond_results" class="clearfix">
  <div style="width:100%; background-repeat: horizontal; background-image: url(http://edmond20.lvr.de/Bilder/streifen.png); height:199px;" align="center">
    <a href="http://www.medienzentrum.schulministerium.nrw.de/medienzentrumundschule/edmond/" target="_blank">
      <img src="http://edmond20.lvr.de/Bilder/header.png" border="0" alt="" 
           style="width: <?php print $element['#ajax'] ? 'auto' : '100%'; ?>;">
    </a>
  </div>
  <table>
    <thead>
      <tr>
        <th><?php print t('Title'); ?></th>
        <th><?php print t('Media type'); ?></th>
        <th><?php print t('Year'); ?></th>
        <th><?php print t('Publisher'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($element['#results'] as $id => $result): ?>
        <?php 
          $result_element = $result->getFormatted();
          print render($result_element); 
        ?>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
<?php endif; ?>
