<?php
/**
 * @file
 * Template for a single SODIS result/hit.
 */
?>
<div class="articlepreview">
  <div class="row">
    <div class="col-md-2 col-xs-3">
      <div class="thumbnailcontainer">
        <?php if (!empty($result['thumbnail'])): ?>
        <img class="img-responsive" alt="" src="<?php print $result['thumbnail']; ?>"/>
        <?php endif; ?>
      </div>
      <p class="resource_type"><?php print $result['resourcetype']; ?></p>
    </div>
    <div class="col-md-10 col-xs-9">
      <div class="row">
        <div class="col-md-12 links">
          <a href="#">Defekten Link melden</a>
          <a class="rating" href="#">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
          </a>
          <span class="tags">0</span>
          <span class="comments">0</span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <span class="provider">
            <?php if (true): ?>
            <img title="EDMOND NRW" alt="Logo EDMOND NRW" src="<?php  ?>"/>
            <?php endif; ?>
          </span>
          <h3><?php print $result['provider']; ?></h3>
          <h2><a href="<?php print $result['origin']; ?>" target="_blank"><?php print $result['title']; ?></a></h2>
          <p><?php print $result['description']; ?></p>
        </div>
      </div>
    </div>
  </div>
  <div class="row"></div>
</div>
