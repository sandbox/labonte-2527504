<?php
/**
 * @file
 * Display error message in case of a failed search request.
 *
 * Variables:
 * - $service: String indicating the search service originating this error.
 * - $exception: The caught exception object.
 */
?>
<div class="lls-search-error">
  <h2><?php print t('Search request failed with error code: %errno', array('errno' => $exception->getCode())); ?></h2>
  <p><?php print $exception->getMessage(); ?></p>
</div>
