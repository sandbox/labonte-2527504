<?php
// $id:$

/**
 * @file
 * Template file for displaying the most recent learn:line NRW resources.
 */
$module_path = drupal_get_path('module', 'lls');
?>
<h2>
  <span class="headline">
    <?php print $element['#block']->subject; ?>
  </span>
</h2>
<div id="lls-most-recent-container">
  <div id="lls-most-recent-loading">
    <img src="<?php print '/' . $module_path . '/img/ajax-loader4.gif'; ?>" alt="Loading content..."/>
  </div>
  <div id="lls-most-recent-content" style="display: none;"></div>
</div>
<p><a class="boxlinkbutton" href="/learnline/search?search=**">mehr ...</a></p>
