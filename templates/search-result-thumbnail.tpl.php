<?php
// $id:$

/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 */

?>
<figure>
  <?php if (is_string($element['#src'])): ?>
    <img src="<?php print $element['#src']; ?>"/>
  <?php endif; ?>
  <?php if (isset($element['#caption']) && is_string($element['#caption'])): ?> 
    <figcaption><?php print $element['#caption']; ?></figcaption>
  <?php endif; ?>
</figure>
