/**
 * @project learn:line NRW
 * @package Drupal.lls
 * @author Matthias Klame <klame@publicplan.de>
 * @author Yannic Labonte <labonte@publicplan.de>
 * @copyright publicplan GmbH (http://www.publicplan.de/)
 */
(function(Drupal, $) {
  // Attach Drupal behviors to be executed when DOM is fully loaded:
  Drupal.behaviors.lls = {
    attach: function (context, settings) {
      // Hide LOGINEO login buttons/links when the user is already logged in:
      if (Drupal.settings.logineo !== undefined && Drupal.settings.logineo.signedin) {
        $('.ll4-logineo-sso-btn').hide();
      }
      // Add toggle slider for advanced search options:
      $('#advanced-search-toggle').on('click', function() {
        $('#advanced-search-container > div').slideToggle({
          complete: function() {
            if ($('#advanced-search-container > div').is(':visible')) {
              $('#advanced-search-toggle > i').removeClass('fa-arrow-circle-down').addClass('fa-arrow-circle-up');
            }
            else {
              $('#advanced-search-toggle > i').removeClass('fa-arrow-circle-up').addClass('fa-arrow-circle-down');
            }
          }
        });
      });
      // Initially hide the advanced search:
      $('#advanced-search-container > div').hide();
    }
  };
})(Drupal, jQuery);
