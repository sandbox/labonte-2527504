(function($) {
    $.widget('lls.searchTab', {
        options: {
            contentArea: '#block-system-main > .content'
        },
        /**
         * Initialize search service tab.
         *
         * @private
         */
        _init: function() {
            var self = this;
            if (this.element.parent().hasClass('active')) {
                this.addCount();
            } else {
                this.loadData();
            }
            // Add click event for tab switching.
            this.element.on('click', function() {
                $('#primary-service').val(self.element.data('search-service')).prop('disabled', false);
                self.element.parent().addClass('active').siblings().removeClass('active');
                $(self.options.contentArea).children().hide();
                $('#' + self.element.data('search-service') + '-results').show();
            });
        },
        /**
         * Load search results.
         */
        loadData: function() {
            var self = this;
            $.ajax(Drupal.settings.lls.background_search_url, {
                method: 'GET',
                data: self._getRequestData(),
                dataType: 'html',
                success: function(data) {
                    $(data).hide().appendTo($(self.options.contentArea));
                    self.addCount();
                }
            });
        },
        _getRequestData: function() {
            var data = {},
                query = window.location.search;
            var queryData = query.substr(1).split('&');

            for (var component in queryData) {
                var match = queryData[component].match(/^(.+)=(.+)/i);
                if (match) {
                    data[match[1]] = decodeURIComponent(match[2]);
                }
            }

            data['page'] = 1;
            data['primary_service'] = this.element.data('search-service');

            return data;
        },
        /**
         * Add results count to the tab's label.
         */
        addCount: function() {
            var totalCount = $('#' + this.element.data('search-service') + '-results').find('.total-count').text();
            this.element.html(this.element.html() + ' (' + totalCount + ')');
        }
    });

    Drupal.behaviors.lls_background_search = {
        attach: function() {
            $('ul.mediaselect > li > a[data-search-service]').each(function() {
                $(this).searchTab();
            });
        }
    };
})(jQuery);
