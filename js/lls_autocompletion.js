/**
 * @project learn:line NRW v4.0
 * @package Drupal.lls
 * @author Yannic Labonte <labonte@publicplan.de>
 * @copyright publicplan GmbH (http://www.publicplan.de/)
 */
(function($) {
  // Attach Drupal behviors to be executed when DOM is fully loaded:
  Drupal.behaviors.lls_autocompletion = {
    attach: function (context, settings) {
      if ($('#search-input').length < 1) return;
      if (Drupal.settings.lls.typeahead) {
        $('#search-input').autocomplete({
          source: Drupal.settings.basePath + 'lls/sodis/autocomplete',
          minLength: 3,
          autoFocus: false,
          position: {
            of: '#search-input',
            my: 'left top',
            at: 'left bottom'
          }
        });
      }
    }
  };
})(jQuery);
