(function ($) {
  $.widget('lls.edutags', {
    /**
     * Predefined options.
     */
    options: {
      windowOptions: 'toolbar=no,menubar=no,width=500,height=690',
      domain: null,
      base_url: null,
      portal: 'LL',
      id: null,
      title: null,
      description: null,
      tags: null,
      data: null,
      tagRowTemplate: '<li title="%title%"><i class="fa fa-tag"></i>%text%</li>',
      //commentRowTemplate: '<li><b>Kommentar von %username%:</b><br>%title%<br>%content%<br></li>'
      commentRowTemplate: '<li>%content%</li>'
    },
    self: null,
    /**
     * Widget initialization.
     *
     * @private
     */
    _create: function () {
      // Initialize variables and fetch information.
      var self = this;
      this.self = self;
      this.options.id = this.element.attr('id');
      this.options.title = this.element.find('.sodis-title').text();
      this.options.description = this.element.find('.sodis-description').text();
      this.options.tags = [];
      this.element.find('.keyword').each(function () {
        self._addTag($(this).text());
      });
      this.element.find('.context').each(function () {
        self._addTag($(this).text());
      });
      this.element.find('.discipline').each(function () {
        self._addTag($(this).text());
      });
      this.element.find('.learningResourceType_orig').each(function () {
        self._addTag($(this).text());
      });
      // Apply edutags data to HTML elements and attach appropriate events.
      this.element.find('.edutags > .tags').html(this.options.data.tags.length);
      this.element.find('.edutags-button-tag').on('click', function () {
        self.tag();
      });
      this.element.find('.edutags > .comments').html(this.options.data.comments.length);
      this.element.find('.edutags-button-comment').on('click', function () {
        self.comment();
      });
      for (var i = 0; i < this.options.data.rating_stars; i++) {
        this.element.find('.edutags > .rating > .fa.fa-star.empty').first().removeClass('empty');
      }
      this.element.find('.edutags > .rating, .edutags > .rating-link').on('click', function () {
        self.rate();
      });
      this.element.find('.edutags > .rating, .edutags > .tags, .edutags > .comments').css('cursor', 'pointer');
      this.createTagList();
      this.createCommentList();
    },
    /**
     * Add a tag to the options.
     *
     * @param tag The tag to add.
     * @private
     */
    _addTag: function (tag) {
      if ($.inArray(tag, this.options.tags) < 0) {
        this.options.tags.push(tag.trim());
      }
    },
    /**
     * Trigger opening the rating window.
     */
    rate: function () {
      this._openWindow('edutags_voting.php', 'Bewerten');
    },
    /**
     * Trigger opening the tagging window.
     */
    tag: function () {
      this._openWindow('edutags_tagging.php', 'Taggen');
    },
    /**
     * Trigger opening the tagging window.
     */
    comment: function () {
      this._openWindow('edutags_comment.php', 'Kommentieren');
    },
    /**
     * Open new window for.
     *
     * @param url
     *   Request URL ending. Usually just the filename, because it's prefixed with domain and base_url options.
     * @param title
     *   Window title to display.
     * @private
     */
    _openWindow: function (url, title) {
      var request = this.options.domain + this.options.base_url + url,
        query = [];
      var params = {
        portal: this.options.portal,
        url: this.element.data('url'),
        nodeTitle: this.options.title,
        nodeDescription: this.options.description,
        nodeTags: this.options.tags.join(',')
      };
      for (var key in params) {
        query.push(key + '=' + encodeURIComponent(params[key]));
      }

      window.open(request + '?' + query.join('&'), title, this.options.windowOptions);
    },
    /**
     * Creates the <li>-Elements for the current element tags.
     */
    createTagList: function () {
      var element = jQuery(this.self.element).find('.tagbox_tags ul'),
        tagObj = null;

      if (typeof this.options.data.tags !== 'undefined') {
        for (var key in this.options.data.tags) {

          tagObj = this.options.data.tags[key];

          if (this.checkForAttributes(tagObj, ['tag'])) {
            element.append(this.options.tagRowTemplate
              .replace('%title%', 'Wurde ' + tagObj.count + ' Mal getaggt')
              .replace('%text%', tagObj.tag.trim())
            );
          }
        }
      }
    },
    /**
     * Creates the <li>-Elements for the current element comments.
     */
    createCommentList: function () {
      var element = jQuery(this.self.element).find('.tagbox_comments ul'),
        commentObj = null;

      if (this.options.data.comments.length > 0) {
        for (var key in this.options.data.comments) {

          commentObj = this.options.data.comments[key];

          //if(this.checkForAttributes(commentObj, [ 'username', 'title', 'content' ])) {
          if (this.checkForAttributes(commentObj, ['content'])) {
            element.append(this.options.commentRowTemplate
              .replace('%username%', commentObj.username)
              .replace('%title%', commentObj.title)
              .replace('%content%', commentObj.content)
            );
          }
        }
      }
    },
    /**
     * Check for the given attributes in the given object.
     *
     * @param obj
     *  The object.
     * @param keys
     *  A object with attribute names.
     * @returns {boolean}
     *  Returns TRUE if the object is valid otherwise FALSE.
     */
    checkForAttributes: function (obj, keys) {
      var status = true;

      for (var key in keys) {
        if (obj.hasOwnProperty([keys[key]]) === false) {
          status = false;
          break;
        }
        else if (obj[keys[key]] === null) {
          status = false;
          break;
        }
        else if (obj[keys[key]] === '') {
          status = false;
          break;
        }
      }

      return status;
    }
  });
})(jQuery);
