/**
 * @project learn:line NRW
 * @package Drupal.lls
 * @author Yannic Labonte <labonte@publicplan.de>
 * @copyright 2016, publicplan GmbH (http://www.publicplan.de/)
 */
(function(Drupal, $) {
  // Attach Drupal behviors to be executed when DOM is fully loaded:
  Drupal.behaviors.lls_get_most_recent_sodis = {
    attach: function (context, settings) {
      var data = {};
      // Load SODIS' most recent resources:
      $.ajax('/lls/sodis/most-recent', {
        method: 'POST',
        data: data,
        success: function (data) {
          $('#lls-most-recent-loading').hide();
          $('#lls-most-recent-content').html(data).slideDown().find('.remote-src').each(function() {
            $(this).remoteImage();
          });
        }
      });
    }
  };
})(Drupal, jQuery);
