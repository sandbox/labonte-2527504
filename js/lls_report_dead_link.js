(function($) {
  $.widget('lls.reportDeadLink', {
    options: {
      confirmText: ''
    },
    _init: function() {
      var result = $(this.element.parents('.articlepreview'));
      this.options.service = result.data('service');
      this.options.id = result.data('id');
      this.options.target = result.data('url');
      var options = this.options;
      this.element.on('click', function() {

        alertify.confirm(options.confirmText.value, function (e) {
          if(e) {

            $.ajax('/lls/report-dead-link', {
              method: 'POST',
              dataType: 'json',
              data: {
                item_service: options.service,
                item_id: options.id,
                item_target: options.target
              },
              success: function(data) {
                alertify.alert('<i class="fa fa-check dead-link-success"></i> ' + data.message.value);
              }
            });
          }
        });
      });
    },
    report: function() {
    }
  });
})(jQuery);
