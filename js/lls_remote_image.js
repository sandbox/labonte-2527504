(function($) {
    $.widget('lls.remoteImage', {
        _init: function() {
            var img_alt = this.element.siblings('img');
            if (img_alt.length > 0) {
                this.element.on('error', function() {
                    $(this).hide();
                    img_alt.show();
                }).attr('src', this.element.data('img-src'));
            }
        }
    });
})(jQuery);
